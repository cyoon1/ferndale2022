import datetime

# Write out relocated GrowClust event catalogs for plotting in GMT
# [Must have run convert_hyposvi_to_catalog.py before to get the input magnitude map]

#catalog_start_time = '2018-01-01T00:00:00'
#in_growclust_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/GrowClust/OUT_EQT_20180101_20220101_VELZHANG/out.growclust_cat'
#in_magmap_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_MagnitudeMap.txt'
#out_growclust_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_GrowClust_VELZHANG_maxdt2.txt'
#out_nonclust_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_NonCluster_GrowClust_VELZHANG_maxdt2.txt'

catalog_start_time = '2022-12-20T00:00:00'
in_growclust_file = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/GrowClust/OUT/out.growclust_cat'
out_dir = '../Catalog/EQT_20221220_20221227/'
out_growclust_file = out_dir+'EQT_20221220_20221227_REAL_GrowClust_maxdt30.txt'
out_nonclust_file = out_dir+'EQT_20221220_20221227_REAL_NonCluster_GrowClust_maxdt30.txt'

# Read in magnitude map
#mag_map = {}
#with open(in_magmap_file, 'r') as fin:
#   for line in fin:
#      split_line = line.split()
#      hsvi_ev_id = int(split_line[0])
#      hinv_mag = float(split_line[3])
#      mag_map[hsvi_ev_id] = hinv_mag

# Output growclust file with magnitudes
catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
fout = open(out_growclust_file, 'w')
fout_nonclust = open(out_nonclust_file, 'w')
with open(in_growclust_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      year = int(split_line[0])
      month = int(split_line[1])
      day = int(split_line[2])
      hour = int(split_line[3])
      minute = int(split_line[4])
      second = float(split_line[5])
      if (second >= 60.0):
         print("BEFORE: ", year, month, day, hour, minute, second)
         second -= 60.0
         minute += 1
         print("AFTER: ", year, month, day, hour, minute, second)
      origin_time_nosec = datetime.datetime(year, month, day, hour, minute)
      origin_delta = datetime.timedelta(seconds=second)
      origin_time = origin_time_nosec + origin_delta
      num_sec = (origin_time - catalog_ref_time).total_seconds()
      lat_deg = split_line[7]
      lon_deg = split_line[8]
      depth_km = split_line[9]
      evid = int(split_line[6])
#      mag = mag_map[evid]
      mag = float(split_line[10])
      nbranch = int(split_line[13]) # if nbranch==1 then not relocated, skip this event
      if (nbranch != 1):
         fout.write(("%f %s %s %s %s %d\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid))
      else:
         fout_nonclust.write(("%f %s %s %s %s %d\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid))
fout.close()
fout_nonclust.close()
