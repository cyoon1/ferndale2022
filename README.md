# Ferndale 2022 Scripts

This repository contains scripts used to generate enhanced earthquake catalogs for the Mendocino Triple Junction, northern California, area, from 2021-12-01 to 2023-06-01, including the 2021 Mw 6.1-6.0 Petrolia and 2022 Mw 6.4 Ferndale earthquakes.

Station data and enhanced earthquake catalogs can be downloaded here: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10621116.svg)](https://doi.org/10.5281/zenodo.10621116)

Journal article: Yoon, C.E. and D. R. Shelly (2024). Distinct yet adjacent earthquake sequences near the Mendocino Triple Junction: 20 December 2021 Mw 6.1 and 6.0 Petrolia, and 20 December 2022 Mw 6.4 Ferndale, The Seismic Record.

## 1.  Install required software

- Data processing workflow was run on a Dell computer running Linux Ubuntu 18.04.6 LTS.
    - CPU: Intel(R) Xeon(R) Gold 5220R @ 2.20GHz, x86-64 architecture, 96 total (2 threads per core, 24 cores per socket, 2 sockets)
    - GPU: Quadro RTX 5000 with 16 MB memory, NVIDIA-SMI 510.47.03, CUDA 11.6.  
    - EikoNet and HypoSVI were run on the GPU. All other processing was done on the CPU.

- Data analysis and visualization software was run on a MacBook Pro (14-inch, 2023) with Apple M2 Max chip, macOS Ventura 13.6.4
    - [GMT](https://www.generic-mapping-tools.org/) 6.4.0 was used to create all maps
    - GMT was installed with [Homebrew](https://brew.sh/) package manager for macOS: `brew install gmt` following instructions at [https://docs.generic-mapping-tools.org/latest/install.html](https://docs.generic-mapping-tools.org/latest/install.html)

- Requires [Anaconda3](https://www.anaconda.com/download#linux) with python 3.10 for Linux (Command Line installer), macOS (Command Line installer)
    - [ObsPy](https://docs.obspy.org/) 1.4.0 was installed and used in the `eqt_dev_latest` and `eiko_svi_latest` conda environments.
    - `eqt_dev_latest` conda environment was also installed and used on the MacBook Pro for data analysis and visualization with matplotlib.

- Requires `gcc` with OpenMP to compile REAL (C code), and `gfortran` to compile GrowClust (Fortran code), on Linux.
- Requires `git` on Linux and Mac, of course!

### 1A. Install EQTransformer from source with a few changes
- Original EQTransformer repo: [https://github.com/smousavi05/EQTransformer](https://github.com/smousavi05/EQTransformer)
- A typical EQTransformer install would start with a cloned repo:
```
(base) yoon@tejon:~/Documents/EQTransformer$ git clone https://github.com/smousavi05/EQTransformer.git .
```
- Our workflow used a forked EQTransformer repo at commit [17b701c](https://github.com/smousavi05/EQTransformer/commit/17b701c) with a few changes: [https://github.com/claraeyoon/EQTransformer](https://github.com/claraeyoon/EQTransformer)
- Clone the forked repo:
```
(base) yoon@tejon:~/Documents/EQTransformer$ git clone https://github.com/claraeyoon/EQTransformer.git .
(base) yoon@tejon:~/Documents/EQTransformer$ git remote -v
origin  git@github.com:claraeyoon/EQTransformer.git (fetch)
origin  git@github.com:claraeyoon/EQTransformer.git (push)
```
- Add the original repo as upstream:
```
(base) yoon@tejon:~/Documents/EQTransformer$ git remote add upstream https://github.com/smousavi05/EQTransformer.git
(base) yoon@tejon:~/Documents/EQTransformer$ git remote -v
origin  git@github.com:claraeyoon/EQTransformer.git (fetch)
origin  git@github.com:claraeyoon/EQTransformer.git (push)
upstream        https://github.com/smousavi05/EQTransformer.git (fetch)
upstream        https://github.com/smousavi05/EQTransformer.git (push)
```
- Check diff with upstream
```
(base) yoon@tejon:~/Documents/EQTransformer$ git diff --name-only upstream/master
EQTransformer/core/EqT_utils.py
EQTransformer/core/mseed_predictor.py
setup.py
```
- Create `eqt_dev_latest` conda environment, then activate
```
(base) yoon@tejon:~/Documents/EQTransformer$ conda create -n eqt_dev_latest python=3.10
(base) yoon@tejon:~/Documents/EQTransformer$ conda activate eqt_dev_latest
(eqt_dev_latest) yoon@tejon:~/Documents/EQTransformer$
```
- Install EQTransformer from source
    - `conda uninstall numpy`: don’t want numpy 1.24 (auto-installed with matplotlib-base) because of an error message, so let `setup.py` install correct numpy version
    - `pip install future`: not needed for EQTransformer, but used in a later script `PARTIALoutput_dtcc_file.py` for pair-wise cross-correlation dtcc file
```
(eqt_dev_latest) yoon@tejon:~/Documents/EQTransformer$ conda install matplotlib-base=3.5.2
(eqt_dev_latest) yoon@tejon:~/Documents/EQTransformer$ conda uninstall numpy
(eqt_dev_latest) yoon@tejon:~/Documents/EQTransformer$ python setup.py install
(eqt_dev_latest) yoon@tejon:~/Documents/EQTransformer$ pip install future
```

### 1B. Install REAL from source with a few changes
- Original REAL repo: [https://github.com/Dal-mzhang/REAL](https://github.com/Dal-mzhang/REAL)
- A typical REAL install would start with a cloned repo:
```
(base) yoon@tejon:~/Documents/REAL$ git clone https://github.com/Dal-mzhang/REAL.git .
```
- Our workflow used a forked REAL repo at commit [6435ace](https://github.com/Dal-mzhang/REAL/commit/6435ace) with a few changes: [https://github.com/claraeyoon/REAL](https://github.com/claraeyoon/REAL)
- Clone the forked repo:
```
(base) yoon@tejon:~/Documents/REAL$ git clone https://github.com/claraeyoon/REAL.git .
(base) yoon@tejon:~/Documents/REAL$ git remote -v
origin  git@github.com:claraeyoon/REAL.git (fetch)
origin  git@github.com:claraeyoon/REAL.git (push)
```
- Add the original repo as upstream:
```
(base) yoon@tejon:~/Documents/REAL$ git remote add upstream https://github.com/Dal-mzhang/REAL.git
(base) yoon@tejon:~/Documents/REAL$ git remote -v
origin  git@github.com:claraeyoon/REAL.git (fetch)
origin  git@github.com:claraeyoon/REAL.git (push)
upstream        https://github.com/Dal-mzhang/REAL.git (fetch)
upstream        https://github.com/Dal-mzhang/REAL.git (push)
```
- Check diff with upstream
```
(base) yoon@tejon:~/Documents/REAL$ git diff --name-only upstream/master
bin/REAL
src/REAL/Makefile
src/REAL/REAL.c
```
- Install REAL from source.  Requires `gcc` compiler and OpenMP.
    - `conda uninstall numpy`: don’t want numpy 1.24 (auto-installed with matplotlib-base) because of an error message, so let `setup.py` install correct numpy version
    - `pip install future`: not needed for EQTransformer, but used in a later script `PARTIALoutput_dtcc_file.py` for pair-wise cross-correlation dtcc file
```
(base) yoon@tejon:~/Documents/REAL$ cd src/REAL
(base) yoon@tejon:~/Documents/REAL/src/REAL$ make
```
- Create symbolic link to REAL in `/usr/local/bin` to run without specifying full path.
```
(base) yoon@tejon:~/Documents/REAL/src/REAL$ cd /usr/local/bin
(base) yoon@tejon:/usr/local/bin$ sudo ln -s /home/yoon/Documents/REAL/bin/REAL REAL
```

### 1C. Install EikoNet and HypoSVI from source with a few changes
- Original EikoNet repo: [https://github.com/Ulvetanna/EikoNet](https://github.com/Ulvetanna/EikoNet)
- A typical EikoNet install would start with a cloned repo:
```
(base) yoon@tejon:~/Documents/EikoNet$ git clone https://github.com/Ulvetanna/EikoNet.git .
```
- Our workflow used a forked EikoNet repo at commit [59dd11f](https://github.com/claraeyoon/EikoNet/commit/59dd11f) without any changes: [https://github.com/claraeyoon/EikoNet](https://github.com/claraeyoon/EikoNet)
- Clone the forked EikoNet repo:
```
(base) yoon@tejon:~/Documents/EikoNet$ git clone https://github.com/claraeyoon/EikoNet.git .
(base) yoon@tejon:~/Documents/EikoNet$ git remote -v
origin  git@github.com:claraeyoon/EikoNet.git (fetch)
origin  git@github.com:claraeyoon/EikoNet.git (push)
```
- Add the original EikoNet repo as upstream:
```
(base) yoon@tejon:~/Documents/EikoNet$ git remote add upstream https://github.com/Ulvetanna/EikoNet.git
(base) yoon@tejon:~/Documents/EikoNet$ git remote -v
origin  git@github.com:claraeyoon/HypoSVI.git (fetch)
origin  git@github.com:claraeyoon/HypoSVI.git (push)
upstream        https://github.com/Ulvetanna/EikoNet.git (fetch)
upstream        https://github.com/Ulvetanna/EikoNet.git (push)
```
- Original HypoSVI repo: [https://github.com/Ulvetanna/HypoSVI](https://github.com/Ulvetanna/HypoSVI)
- A typical HypoSVI install would start with a cloned repo:
```
(base) yoon@tejon:~/Documents/HypoSVI$ git clone https://github.com/Ulvetanna/HypoSVI.git .
```
- Our workflow used a forked HypoSVI repo at commit [4d44957](https://github.com/claraeyoon/HypoSVI/commit/4d44957) with a few changes: [https://github.com/claraeyoon/HypoSVI](https://github.com/claraeyoon/HypoSVI)
- Clone the forked HypoSVI repo:
```
(base) yoon@tejon:~/Documents/HypoSVI$ git clone https://github.com/claraeyoon/HypoSVI.git .
(base) yoon@tejon:~/Documents/HypoSVI$ git remote -v
origin  git@github.com:claraeyoon/HypoSVI.git (fetch)
origin  git@github.com:claraeyoon/HypoSVI.git (push)
```
- Add the original HypoSVI repo as upstream:
```
(base) yoon@tejon:~/Documents/EQTransformer$ git remote add upstream https://github.com/Ulvetanna/HypoSVI.git
(base) yoon@tejon:~/Documents/EQTransformer$ git remote -v
origin  git@github.com:claraeyoon/HypoSVI.git (fetch)
origin  git@github.com:claraeyoon/HypoSVI.git (push)
upstream        https://github.com/Ulvetanna/HypoSVI.git (fetch)
upstream        https://github.com/Ulvetanna/HypoSVI.git (push)
```
- Check diff with upstream. Most important changes are to `HypoSVI/location.py`.
```
(base) yoon@tejon:~/Documents/HypoSVI$ git diff --name-only upstream/master 
HypoSVI.egg-info/PKG-INFO
HypoSVI.egg-info/SOURCES.txt
HypoSVI/location.py
build/lib/HypoSVI/location.py
dist/HypoSVI-1.0.0-py3.7.egg
dist/HypoSVI-1.0.0-py3.8.egg
```
- Create `eiko_svi_latest` conda environment, then activate
```
(base) yoon@tejon:~/Documents$ conda create -n eiko_svi_latest python=3.10
(base) yoon@tejon:~/Documents$ conda activate eiko_svi_latest
(eiko_svi_latest) yoon@tejon:~/Documents$
```
- Install libraries and dependencies first - otherwise will get errors when installing EikoNet and HypoSVI from source
```
(eiko_svi_latest) yoon@tejon:~/Documents$ conda install obspy
(eiko_svi_latest) yoon@tejon:~/Documents$ conda install cudnn
(eiko_svi_latest) yoon@tejon:~/Documents$ conda install scikit-learn
(eiko_svi_latest) yoon@tejon:~/Documents$ conda install seaborn
(eiko_svi_latest) yoon@tejon:~/Documents$ conda install ipython
(eiko_svi_latest) yoon@tejon:~/Documents$ pip install scikit-fmm
```
- Install EikoNet from source
```
(eiko_svi_latest) yoon@tejon:~/Documents$ cd EikoNet
(eiko_svi_latest) yoon@tejon:~/Documents/EikoNet$ python setup.py install
(eiko_svi_latest) yoon@tejon:~/Documents/EikoNet$ cd ..
```
- Install HypoSVI from source
```
(eiko_svi_latest) yoon@tejon:~/Documents$ cd HypoSVI
(eiko_svi_latest) yoon@tejon:~/Documents/HypoSVI$ python setup.py install
(eiko_svi_latest) yoon@tejon:~/Documents/HypoSVI$ cd ..
```

### 1D. Install GrowClust from source with a few changes
- Original GrowClust repo: [https://github.com/dttrugman/GrowClust](https://github.com/dttrugman/GrowClust)
- A typical GrowClust install would start with a cloned repo:
```
(base) yoon@tejon:~/Documents/GrowClust$ git clone https://github.com/dttrugman/GrowClust.git .
```
- Our workflow used a forked GrowClust repo at commit [71f584d](https://github.com/dttrugman/GrowClust/commit/71f584d) with a few changes: [https://github.com/claraeyoon/GrowClust](https://github.com/claraeyoon/GrowClust)
- Clone the forked repo:
```
(base) yoon@tejon:~/Documents/GrowClust$ git clone https://github.com/claraeyoon/GrowClust.git .
(base) yoon@tejon:~/Documents/GrowClust$ git remote -v
origin  git@github.com:claraeyoon/GrowClust.git (fetch)
origin  git@github.com:claraeyoon/GrowClust.git (push)
```
- Add the original repo as upstream:
```
(base) yoon@tejon:~/Documents/GrowClust$ git remote add upstream https://github.com/dttrugman/GrowClust.git
(base) yoon@tejon:~/Documents/GrowClust$ git remote -v
origin  git@github.com:claraeyoon/GrowClust.git (fetch)
origin  git@github.com:claraeyoon/GrowClust.git (push)
upstream        https://github.com/dttrugman/GrowClust.git (fetch)
upstream        https://github.com/dttrugman/GrowClust.git (push)
```
- Check diff with upstream
```
(base) yoon@tejon:~/Documents/GrowClust$ git diff --name-only upstream/master
SRC/Makefile
SRC/grow_params.f90
```
- Install GrowClust from source.  Requires `gfortran` compiler.
```
(base) yoon@tejon:~/Documents/GrowClust$ cd SRC
(base) yoon@tejon:~/Documents/GrowClust/SRC$ make
```
- Create symbolic link to GrowClust in `/usr/local/bin` to run without specifying full path.
```
(base) yoon@tejon:~/Documents/GrowClust/SRC$ cd /usr/local/bin
(base) yoon@tejon:/usr/local/bin$ sudo ln -s /home/yoon/Documents/GrowClust/SRC/growclust growclust
```


## 2. Data processing workflow: generate enhanced catalog

### 2A. Get ComCat earthquake catalog for comparison

- Enter directory with scripts, activate `eqt_dev_latest` conda environment
```
(base) yoon@tejon:~$ cd ~/Documents/Ferndale2022/Scripts/
(base) yoon@tejon:~/Documents/Ferndale2022/Scripts$ conda activate eqt_dev_latest
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ 
```

- Run [get_catalog_ferndale.sh](get_catalog_ferndale.sh) to get ComCat earthquake catalog as text file
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ ./get_catalog_ferndale.sh
t_start='2021-12-01T00:00:00'
t_end='2023-06-01T00:00:00'
out_file='../Catalog/EQT_20211201_20230601/catalog_ferndale_20211201_20230601_download20230705.txt'
min_lat=39.5
max_lat=41.5
min_lon=-126
max_lon=-123
out_format='text'
data_src='https://earthquake.usgs.gov/fdsnws/event/1/query'
query_str=${data_src}?format=${out_format}\&minlatitude=${min_lat}\&maxlatitude=${max_lat}\&minlongitude=${min_lon}\&maxlongitude=${max_lon}\&starttime=${t_start}\&endtime=${t_end}\&orderby='time-asc'

(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ cd ../Catalog/EQT_20211201_20230601/
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Catalog/EQT_20211201_20230601$ wc -l catalog_ferndale_20211201_20230601_download20230705.txt 
1192 catalog_ferndale_20211201_20230601_download20230705.txt
```

- Run [get_catalog_ferndale.py](get_catalog_ferndale.py) to get ComCat earthquake catalog as eventid.xml files with phases - later used in missed event analysis
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python get_catalog_ferndale.py
client = Client("USGS")
min_lat=39.5
max_lat=41.5
min_lon=-126
max_lon=-123
min_mag = -2
t_start = UTCDateTime("2021-12-01T00:00:00")
t_end = UTCDateTime("2023-06-01T00:00:00")
out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/CatalogEventsDownload20230705/'
Number of Events not downloaded:  0
Events not downloaded:  []

(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ cd /media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/CatalogEventsDownload20230705/
(eqt_dev_latest) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/CatalogEventsDownload20230705$ ls *.xml|wc -l
1190
```

- Run [convert_comcat_catalog.py](convert_comcat_catalog.py) to convert ComCat catalog to plain text format for easier plotting
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python convert_comcat_catalog.py 
cat_dir = '../Catalog/EQT_20211201_20230601/'
in_comcat_file = cat_dir+'catalog_ferndale_20211201_20230601_download20230705.txt'
out_catalog_file = cat_dir+'catalog_new_ferndale_20211201_20230601_download20230705.txt'
catalog_start_time='2021-12-01T00:00:00'
Missing magnitude estimate, skip:  ['nc73665126', '2021-12-18T09:02:16.180', '40.482', '-124.4248333', '26.97', 'nc', 'nc', 'nc', 'nc73665126', '', '', 'nc', '17km SW of Ferndale, CA\n']
```

- Run [magnitude_to_circular_radius.py](magnitude_to_circular_radius.py) to add circular rupture radius to last column of plain text ComCat catalog for plotting
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python magnitude_to_circular_radius.py 
cat_dir = '../Catalog/EQT_20211201_20230601/'
input_file = cat_dir+'catalog_new_ferndale_20211201_20230601_download20230705.txt'
output_file = cat_dir+'catalog_new_ferndale_20211201_20230601_download20230705_diam.txt'
```

### 2B. Get relevant stations and download continuous seismic data

- Run [eqt_get_station_list.py](eqt_get_station_list.py) to get station list in JSON format; calls [downloader.py](downloader.py)
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python eqt_get_station_list.py 
clientlist=["NCEDC", "IRIS"]
minlat=39.5
maxlat=41.5
minlon=-126.0
maxlon=-123.0
chan_priority_list=["HH[ZNE12]", "BH[ZNE12]", "EH[ZNE12]", "HN[ZNE12]"]
tstart="2021-12-01 00:00:00.00"
tend="2023-06-01 00:00:00.00"
out_station_json_file='/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/station_list.json'
```

- Run [eqt_parallel_download_mseed_waveforms.py](eqt_parallel_download_mseed_waveforms.py) to download MiniSEED continuous waveforms; one sub-folder per day on parallel CPUs (20211201, 20211202, ..., 20230530, 20230531) saved to `out_base_mseed_dir`.  Calls [eqt_download_mseed_waveforms.py](eqt_download_mseed_waveforms.py)
    - *Note: may need to run this script [eqt_parallel_download_mseed_waveforms.py](eqt_parallel_download_mseed_waveforms.py) multiple times to download all waveforms*
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python eqt_parallel_download_mseed_waveforms.py 
day_start="20211201”
day_end="20230601”
out_base_mseed_dir = "/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/downloads_mseeds/"
delta = datetime.timedelta(days=1) # one sub-directory per day
folder_format = "%Y%m%d"
num_cores = 4
#—> Calls eqt_download_mseed_waveforms.py
clientlist=["NCEDC", "IRIS"]
minlat=39.5
maxlat=41.5
minlon=-126.0
maxlon=-123.0
chan_priority_list=["HH[ZNE12]", "BH[ZNE12]", "EH[ZNE12]", "HN[ZNE12]"]
in_station_json_file='/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/station_list.json'
Sub-folders: 20211201, 20211202, …, 20230530, 20230531
```

- Run [list_number_of_mseed_files.sh](list_number_of_mseed_files.sh) to check that all available files were properly downloaded: Number of MiniSEED files for each day should match the expected number.
    - Outputs `number_of_total_mseed_files.txt`, `number_of_mseed_files.txt` files.
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ ./list_number_of_mseed_files.sh
cd /media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/downloads_mseeds
```

- Run [output_zeropad_mseed_files.py](output_zeropad_mseed_files.py) to zero-pad each daylong MiniSEED waveform by 2 hours at the end (to avoid missing picks in last hour, 23:00:00 to 24:00:00)
    - Outputs a zero-padded copy of each original MiniSEED file in `zeropad_mseeds/` directory.  Does not overwrite original MiniSEED files in `downloads_mseeds/` directory, but requires a lot of disk space.
```
(eqt_dev_latest) yoon@tejon:~/Documents/PuertoRico/Scripts$ python output_zeropad_mseed_files.py 
base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/'
in_mseed_str = 'downloads_mseeds'
out_mseed_str = 'zeropad_mseeds'
zeropad_seconds = 7200 # 2 hours
```

- Check that number of MiniSEED files match in `downloads_mseeds/` and `zeropad_mseeds/` directories
```
(eqt_dev_latest) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/downloads_mseeds$ find . -name "*.mseed" | wc -l
51337
(eqt_dev_latest) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/zeropad_mseeds$ find . -name "*.mseed" | wc -l
51337
```

### 2C. Run EQTransformer - event detection and phase picking

- Run EQTransformer detector/picker with [eqt_parallel_run_mseed_predictor.py](eqt_parallel_run_mseed_predictor.py); one sub-folder per day on parallel CPUs; calls [eqt_run_mseed_predictor.py](eqt_run_mseed_predictor.py)
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python eqt_parallel_run_mseed_predictor.py 
day_start="20211201"
day_end="20230601"
base_dir = "/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/"
in_base_mseed_dir = base_dir+"zeropad_mseeds/"
out_base_det_dir = base_dir+"mseed_detections/"
delta = relativedelta(days=1) # one sub-directory per day
folder_format = "%Y%m%d"
num_cores = 48
#—> Calls eqt_run_mseed_predictor.py
model_path="/home/yoon/Documents/EQTransformer/ModelsAndSampleData/"
MODEL_FILE='EqT_model_conservative.h5' # initial model - minimize false positives
DET_THRESH = 0.05
P_THRESH = 0.03
S_THRESH = 0.03
OVER_LAP = 0.9 # overlap fraction between adjacent time windows
BATCH = 500 
NUM_PLOTS = 0 
in_station_json_file='/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/station_list.json'
# Sub-folders: 20211201, 20211202, …, 20230530, 20230531
```

- Check number of lines in all EQTransformer `.csv` output files, check that number of `.csv` files matches number of directories
```
(eqt_dev_latest) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/mseed_detections$ wc -l */*/*.csv
  1060630 total
(eqt_dev_latest) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/mseed_detections$ ls -l 20*/*/*.csv|wc -l
20501
(eqt_dev_latest) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/mseed_detections$ ls -d ../zeropad_mseeds/20*/*|wc -l
20501
```

- Run [EQTransform2REAL.py](EQTransform2REAL.py) to convert picks from EQTransformer csv format to REAL input format (one folder per day, one file per station/phase); calls `EQTransform2DataFrame()` from [utils_eqt.py](utils_eqt.py)
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python EQTransform2REAL.py 
base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/'
in_station_file = base_dir+'station_list.json'
out_station_file = base_dir+'station_list_edited.json' # only stations with picks
in_pick_dir = base_dir+'mseed_detections/'
out_pick_dir = base_dir+'REAL/Pick/'
tstart="2021-12-01 00:00:00.00"
tend="2023-06-01 00:00:00.00"
Runtime for EQTransform2REAL convert_picks:  391.4466857910156
Number of total stations in station_list.json =  78
Number of stations with picks in station_list_edited.json =  66
```

- Check number of P, S, total picks going into REAL
```
(eqt_dev_latest) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL$ wc -l Pick/202*/*.txt
 1657707 total
(eqt_dev_latest) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL$ wc -l Pick/202*/*P.txt
  823177 total
 (eqt_dev_latest) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL$ wc -l Pick/202*/*S.txt
  834530 total
```

### 2D. Get station xml and input files

- Note: These scripts to get station files require the `station_list_edited.json` output from [EQTransform2REAL.py](EQTransform2REAL.py).

- Run [output_stationxml.py](output_stationxml.py) to output StationXML files - they have instrument response needed later for magnitude estimates
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python output_stationxml.py 
   base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/'
   in_sta_file = base_dir+'station_list_edited.json'
   out_base_dir = base_dir+'StationXML/'
   start_time = UTCDateTime("2021-12-01T00:00:00")
   end_time = UTCDateTime("2023-06-01T00:00:00")
   client = Client("NCEDC")
```

- Run [output_station_file.py](output_station_file.py) to output station files in various input formats for other software. Output folders need to be created before running this script. Total 66 stations.
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python output_station_file.py 
   base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/'
   in_sta_file = base_dir+'station_list_edited.json'
   out_hypoinv_file1 = base_dir+'mendo_stations.sta'
   out_growclust_file = base_dir+'REAL/GrowClust/IN/mendo_stations_GrowClust.txt'
   out_real_file = base_dir+'REAL/mendo_stations.txt'
   out_gmt_file = '../Catalog/EQT_20211201_20230601/EQT_20211201_20230601_mendo_stations.txt'
   out_hyposvi_file = base_dir+'Eiko_out_MTJ/mendo_st.out'
```

### 2E. Run REAL - event association

- Run [taup_tt.py](taup_tt.py) to create REAL travel time table based on input 1D velocity model in [vel_mendo.nd](config/REAL/tt_db/vel_mendo.nd) config file
```
(eqt_dev_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python taup_tt.py
tt_dir = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL/tt_db/'
tt_out_file = tt_dir+'ttdb_mendo.txt'
build_taup_model(tt_dir+"vel_mendo.nd")
model = TauPyModel(model="vel_mendo")
dep=40 #depth in km
ddep=1 #depth interval, be exactly divided by dep
dist=2.0 #dist range in deg.
ddist=0.01 #dist interval, be exactly divided by dist
```

- Run REAL using the script [runREAL.sh](runREAL.sh).  
    - REAL runs on one day of picks at a time; [runREAL.sh](runREAL.sh) can be called with input arguments to run one day after another for a given start date year-month-day and number of days. The given example's start date is 2023-01-01, and runs for the next 30 days until 2023-01-31 (inclusive).
    - The output directory `realdir` and its subdirectory `outeventdir` must exist beforehand, or else REAL will output an error message when writing to disk.
    - The script [runREAL.sh](runREAL.sh) calls the REAL executable in `/usr/local/bin` where we made a symbolic link to the original REAL executable.
```
(base) yoon@tejon:~/Documents/Ferndale2022/Scripts$ ./runREAL.sh 2023 01 01 30
export OMP_NUM_THREADS=84
R="0.7/40/0.02/2/5/360/180/40.5/-124.3"
G="2.0/40/0.01/1"
V="6.46/3.63"
S="3/3/6/3/0.5/0.2/1.5"
LAT_CENTER=40.5
realdir="/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL"
station=${realdir}"/mendo_stations.txt"
ttime=${realdir}"/tt_db/ttdb_mendo.txt"
outeventdir="Events"
```

- Run REAL for all days using the script [run_all_REAL.sh](run_all_REAL.sh) which has multiple calls to [runREAL.sh](runREAL.sh)
    - Note: REAL has the slowest runtime in the entire workflow, since it is a grid search associator.  It took >1 week to finish processing this data set on multiple CPUs and computers.  I recommend using as many CPUs as possible on a given computer, set by the `OMP_NUM_THREADS` parameter in [runREAL.sh](runREAL.sh), and also running REAL on as many different computers as one can access.
```
(base) yoon@tejon:~/Documents/Ferndale2022/Scripts$ ./run_all_REAL.sh
```

- Check REAL outputs in `outeventdir`: number of files should equal number of days; get total number of associated events.
```
(base) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL/Events$ ls 2*phase*|wc -l
547
(base) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL/Events$ wc -l 2*catalog*
  18940 total
```

- REAL outputs one catalog file and one phase file per day; concatenate them to get one catalog file and one phase file for the entire time period.
```
(base) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL/Events$ cat *catalog_sel.txt > EQT_20211201_20230601_catalog_sel.txt
(base) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL/Events$ cat *phase_sel.txt > EQT_20211201_20230601_phase_sel.txt
```

### 2F. Run EikoNet and HypoSVI - absolute event location

- Copy 1D velocity model input files [VP_MENDO.csv](config/Eiko/VP_MENDO/VP_MENDO.csv) and [VS_MENDO.csv](config/Eiko/VS_MENDO/VS_MENDO.csv), from config folder, into subdirectories in the EikoNet-HypoSVI output folder: `/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/Eiko_out_MTJ/`
```
(base) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/Eiko_out_MTJ$ cp -r ~/Documents/Ferndale2022/Scripts/config/Eiko/VP_MENDO/ ./VP_MENDO/
(base) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/Eiko_out_MTJ$ cp -r ~/Documents/Ferndale2022/Scripts/config/Eiko/VS_MENDO/ ./VS_MENDO/
```

- Enter directory with scripts, activate `eiko_svi_latest` conda environment
```
(base) yoon@tejon:~$ cd ~/Documents/Ferndale2022/Scripts/
(base) yoon@tejon:~/Documents/Ferndale2022/Scripts$ conda activate eiko_svi_latest
(eiko_svi_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ 
```

- Run [train_eiko.py](train_eiko.py) to train an EikoNet for the input 1D velocity models; takes ~20 minutes to run on GPU.
    - We selected the model at epoch 15, with the lowest validation loss for both VP and VS, for further use in HypoSVI.
```
(eiko_svi_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python train_eiko.py
PATH = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/Eiko_out_MTJ'
xmin               = [-126.0,39.5,-2] #Lat,Long,Depth
xmax               = [-123.0,41.5,41] #Lat,Long,Depth
projection         = "+proj=utm +zone=10 +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"
vp_dir = 'VP_MENDO'
vp_file = vp_dir+'/VP_MENDO.csv'
vs_dir = 'VS_MENDO'
vs_file = vs_dir+'/VS_MENDO.csv'
model_VP.Params['Training']['Number of Epochs']   = 16 # For the 1D velocity 10 epochs could be overkill
model_VP.Params['Training']['Save Every * Epoch'] = 1 # For the 1D velocity 10 epochs could be overkill
model_VS.Params['Training']['Number of Epochs']   = 16 # For the 1D velocity 10 epochs could be overkill
model_VS.Params['Training']['Save Every * Epoch'] = 1 # For the 1D velocity 10 epochs could be overkill
(1000000, 6) (1000000, 2)
cuda:0
=======================================================================================
=======================================================================================
========================== Eikonal Solver - Training ==================================
=======================================================================================
=======================================================================================
=======================================================================================
Epoch = 1 -- Training loss = 2.6600e-01 -- Validation loss = 6.0506e-02
Epoch = 2 -- Training loss = 5.2267e-02 -- Validation loss = 3.4950e-02
Epoch = 3 -- Training loss = 3.5469e-02 -- Validation loss = 2.0458e-02
Epoch = 4 -- Training loss = 2.4384e-02 -- Validation loss = 1.6545e-02
Epoch = 5 -- Training loss = 2.3813e-02 -- Validation loss = 1.9505e-02
Epoch = 6 -- Training loss = 2.3269e-02 -- Validation loss = 1.0841e-02
Epoch = 7 -- Training loss = 2.0137e-02 -- Validation loss = 1.4723e-02
Epoch = 8 -- Training loss = 1.7817e-02 -- Validation loss = 1.0065e-02
Epoch = 9 -- Training loss = 1.6448e-02 -- Validation loss = 2.0143e-02
Epoch = 10 -- Training loss = 1.5836e-02 -- Validation loss = 1.6990e-02
Epoch = 11 -- Training loss = 1.3866e-02 -- Validation loss = 1.4679e-02
Epoch = 12 -- Training loss = 1.2755e-02 -- Validation loss = 9.0960e-03
Epoch = 13 -- Training loss = 1.1887e-02 -- Validation loss = 1.2246e-02
Epoch = 14 -- Training loss = 1.0667e-02 -- Validation loss = 1.2830e-02
Epoch = 15 -- Training loss = 9.4322e-03 -- Validation loss = 7.0797e-03 #-> Select Model_Epoch_00015_ValLoss_0.007079711141891049.pt
Epoch = 16 -- Training loss = 9.7021e-03 -- Validation loss = 7.8124e-03
(1000000, 6) (1000000, 2)
cuda:0
=======================================================================================
=======================================================================================
========================== Eikonal Solver - Training ==================================
=======================================================================================
=======================================================================================
=======================================================================================
Epoch = 1 -- Training loss = 9.3519e-02 -- Validation loss = 1.8466e-02
Epoch = 2 -- Training loss = 2.1881e-02 -- Validation loss = 1.3400e-02
Epoch = 3 -- Training loss = 2.1103e-02 -- Validation loss = 1.8089e-02
Epoch = 4 -- Training loss = 1.8676e-02 -- Validation loss = 1.0247e-02
Epoch = 5 -- Training loss = 1.5469e-02 -- Validation loss = 1.0697e-02
Epoch = 6 -- Training loss = 1.5354e-02 -- Validation loss = 1.8094e-02
Epoch = 7 -- Training loss = 1.3719e-02 -- Validation loss = 7.8685e-03
Epoch = 8 -- Training loss = 1.2789e-02 -- Validation loss = 1.4436e-02
Epoch = 9 -- Training loss = 1.0524e-02 -- Validation loss = 9.1662e-03
Epoch = 10 -- Training loss = 1.0423e-02 -- Validation loss = 1.2331e-02
Epoch = 11 -- Training loss = 9.3345e-03 -- Validation loss = 5.2492e-03
Epoch = 12 -- Training loss = 8.8152e-03 -- Validation loss = 9.7883e-03
Epoch = 13 -- Training loss = 8.9412e-03 -- Validation loss = 4.7133e-03
Epoch = 14 -- Training loss = 8.0513e-03 -- Validation loss = 4.8716e-03
Epoch = 15 -- Training loss = 7.5580e-03 -- Validation loss = 5.0841e-03 #-> Select Model_Epoch_00015_ValLoss_0.0050841319948939.pt
Epoch = 16 -- Training loss = 7.2362e-03 -- Validation loss = 8.1733e-03
```

- Run [REAL2hyposvi.py](REAL2hyposvi.py) to convert REAL outputs to HypoSVI (NonLinLoc) input format
    - `PickError` (seconds) determined from EQT/REAL pick probability (0-1) converted to HYPOINVERSE pick weights (0-4), defined in [utils_pickweight.py](utils_pickweight.py)
```
(eiko_svi_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python REAL2hyposvi.py 
base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/'
in_real_phase_file = base_dir+'REAL/Events/EQT_20211201_20230601_phase_sel.txt'
out_hinv_phase_file = base_dir+'Eiko_out_MTJ/EQT_20211201_20230601_REAL_PickErrorWEIGHT.nlloc'
Number of events in HYPOSVI input file: 18940
```

- Run HypoSVI to locate events with script [svi.py](svi.py). Takes 3-7 seconds per event to run on GPU; this dataset ran in ~48 hours.
    - Output png file for each event with location and picks on waveform files (run slower)
```
(eiko_svi_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python svi.py > EQT_20211201_20230601_output_svi.txt
PATH = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/Eiko_out_MTJ'
xmin               = [-126.0,39.5,-2] #Lat,Long,Depth
xmax               = [-123.0,41.5,41] #Lat,Long,Depth
projection         = "+proj=utm +zone=10 +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"
vp_dir = 'VP_MENDO'
vp_file = vp_dir+'/VP_MENDO.csv'
vp_model_file = 'Model_Epoch_00015_ValLoss_0.007079711141891049.pt'
vs_dir = 'VS_MENDO'
vs_file = vs_dir+'/VS_MENDO.csv'
vs_model_file = 'Model_Epoch_00015_ValLoss_0.0050841319948939.pt'
in_event_file = 'EQT_20211201_20230601_REAL_PickErrorWEIGHT.nlloc'
out_hyposvi_dir = 'EQT_20211201_20230601_REAL_PickErrorWEIGHT_Events'
start_event_id = 1000000
Stations       = pd.read_csv('{}/mendo_st.out'.format(PATH),sep=r'\s+')
LocMethod.plot_info['EventPlot']['Traces']['Plot Traces']     = True
LocMethod.plot_info['EventPlot']['Traces']['Trace Host']      = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/downloads_mseeds'
LocMethod.plot_info['EventPlot']['Traces']['Trace Host Type'] = 'EQTransformer'
LocMethod.plot_info['EventPlot']['Traces']['Channel Types']   = ['EH*','HH*','BH*','HN*']
LocMethod.LocateEvents(EVT, Stations, output_plots= True, timer=True, output_path='{}'.format(PATH_EVT))
LocMethod.CataloguePlot(filepath='{}/CatalogPlot.png'.format(PATH_EVT),Events=lc.IO_JSON('{}/Catalogue.json'.format(PATH_EVT),rw_type='r'),user_xmin=[None,None,None],user_xmax=[None,None,None])
```

- Run [plot_hyposvi_locations.py](plot_hyposvi_locations.py) to output HypoSVI locations and uncertainties in csv form
```
(eiko_svi_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python plot_hyposvi_locations.py 
PATH = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/Eiko_out_MTJ'
xmin               = [-126.0,39.5,-2] #Lat,Long,Depth
xmax               = [-123.0,41.5,41] #Lat,Long,Depth
projection         = "+proj=utm +zone=10 +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"
vp_dir = 'VP_MENDO'
vp_file = vp_dir+'/VP_MENDO.csv'
vp_model_file = 'Model_Epoch_00015_ValLoss_0.007079711141891049.pt'
vs_dir = 'VS_MENDO'
vs_file = vs_dir+'/VS_MENDO.csv'
vs_model_file = 'Model_Epoch_00015_ValLoss_0.0050841319948939.pt'
in_event_file = 'EQT_20211201_20230601_REAL_PickErrorWEIGHT.nlloc'
in_hyposvi_dir = 'EQT_20211201_20230601_REAL_PickErrorWEIGHT_Events'
out_catalog_plot_file = 'EQT_20211201_20230601_REAL_DIRECTHYPOSVI_Catalog.png'
out_catalog_csv_file = 'EQT_20211201_20230601_REAL_DIRECTHYPOSVI_Catalog.csv'
EVT_org = lc.IO_JSON('{}/Catalogue.json'.format(PATH_EVT),rw_type='r')
Stations       = pd.read_csv('{}/mendo_st.out'.format(PATH),sep=r'\s+')
```

- Check number of events located by HypoSVI
```
(base) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/Eiko_out_MTJ$ wc -l EQT_20211201_20230601_REAL_PickErrorWEIGHT_Events/EQT_20211201_20230601_REAL_DIRECTHYPOSVI_Catalog.csv 
18912 EQT_20211201_20230601_REAL_PickErrorWEIGHT_Events/EQT_20211201_20230601_REAL_DIRECTHYPOSVI_Catalog.csv
```

- Run [HYPOSVI2hypoinverse.py](HYPOSVI2hypoinverse.py) to convert HypoSVI csv output to HYPOINVERSE arc file format (for compatibility with other scripts)
   - Updated with HYPOINVERSE pick weight (integer 0-4) from HypoSVI `PickError` (seconds) from EQT/REAL pick probability (0-1), defined in [utils_pickweight.py](utils_pickweight.py)
   - Uses functions from [utils_hypoinverse.py](utils_hypoinverse.py)
   - `out_hinv_dir` directory needs to exist before calling this script
```
(eiko_svi_latest) yoon@tejon:~/Documents/Ferndale2022/Scripts$ python HYPOSVI2hypoinverse.py 
base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/'
out_hinv_dir = base_dir+'REAL/DIRECTHYPOSVI/'
PATH_EVT = base_dir+'Eiko_out_MTJ/EQT_20211201_20230601_REAL_PickErrorWEIGHT_Events'
out_hinv_arc_file = out_hinv_dir+'EQT_20211201_20230601_hyposvi_real_locate_mendo.arc'
out_hinv_sum_file = out_hinv_dir+'EQT_20211201_20230601_hyposvi_real_locate_mendo.sum'
Number of HYPOSVI events written out to HYPOINVERSE arc file:  18911
```

- Concatenate updates for event locations from HypoSVI
   - Sometimes HypoSVI may run on one day at a time or one week at a time, and we want to concatenate the catalogs from different dates together.
   - This is not the case here, since the entire catalog was created at once, after the earthquakes.
```
(base) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL/DIRECTHYPOSVI$ cp EQT_20211201_20230601_hyposvi_real_locate_mendo.arc hyposvi_real_locate_mendo.arc
(base) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL/DIRECTHYPOSVI$ cp EQT_20211201_20230601_hyposvi_real_locate_mendo.sum hyposvi_real_locate_mendo.sum
(base) yoon@tejon:/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL/DIRECTHYPOSVI$ wc -l hyposvi_real_locate_mendo.sum 
18911 hyposvi_real_locate_mendo.sum
```

### 2G. Calculate local magnitudes


## 3. Data analysis and visualization
X
