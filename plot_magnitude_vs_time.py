import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams
import utils_distance as utils_dist

rcParams.update({'font.size': 24})
#rcParams.update({'font.size': 36}) #poster
###rcParams['pdf.fonttype'] = 42
rcParams['font.sans-serif'] = "Helvetica"
rcParams['font.family'] = "sans-serif"



def plot_mag_vs_time(times_cat, times_eqt, times_missed, mag_cat, mag_eqt, mag_missed, out_plot_magvstime_file):
#   x_rot = 0
#   plt.figure(num=0, figsize=(24,6))
   x_rot = 20
   plt.figure(num=0, figsize=(32,7))
   plt.clf()
#   plt.plot(times_eqt, mag_eqt, 'o', markeredgecolor='c', markerfacecolor='None', markeredgewidth=0.001, label=str(len(times_eqt))+' new EQTransformer events')
#   plt.plot(times_cat, mag_cat, 'o', markeredgecolor='b', markerfacecolor='None', markeredgewidth=0.001, label=str(len(times_cat))+' detected catalog events')
#   plt.plot(times_missed, mag_missed, 'o', markeredgecolor='r', markerfacecolor='None', markeredgewidth=0.001, label=str(len(times_missed))+' missed catalog events')
   ax = plt.gca()
   ax.set_facecolor((0.8, 0.8, 0.8))
   plt.scatter(times_cat, mag_cat, s=20*np.multiply(mag_cat, mag_cat), marker='o', facecolors='none', edgecolors='b', linewidths=0.1, label=str(len(times_cat))+' detected catalog events')
   plt.scatter(times_eqt, mag_eqt, s=20*np.multiply(mag_eqt, mag_eqt), marker='o', facecolors='none', edgecolors='cyan', linewidths=0.1, label=str(len(times_eqt))+' new EQTransformer events')
   plt.scatter(times_missed, mag_missed, s=20*np.multiply(mag_missed, mag_missed), marker='o', facecolors='none', edgecolors='r', linewidths=0.1, label=str(len(times_missed))+' missed catalog events')
#   times_allcat = np.concatenate([times_cat, times_missed])
#   mag_allcat = np.concatenate([mag_cat, mag_missed])
#   plt.scatter(times_allcat, mag_allcat, s=20*np.multiply(mag_allcat, mag_allcat), marker='o', facecolors='none', edgecolors='b', linewidths=0.1, label=str(len(times_allcat))+' catalog events')
#   times_alleqt = np.concatenate([times_cat, times_eqt])
#   mag_alleqt = np.concatenate([mag_cat, mag_eqt])
#   plt.scatter(times_alleqt, mag_alleqt, s=20*np.multiply(mag_alleqt, mag_alleqt), marker='o', facecolors='none', edgecolors='b', linewidths=0.1, label=str(len(times_alleqt))+' EQTransformer events')
   plt.yticks([0, 2, 4, 6])
#   plt.legend(loc='upper right', fontsize=20, numpoints=1, handletextpad=0.1)
#   plt.xticks(86400*np.arange(0,8), ['2020-01-07', '2020-01-08', '2020-01-09', '2020-01-10', '2020-01-11', '2020-01-12', '2020-01-13', '2020-01-14'])
#   plt.xlim([0, 604800])

#   plt.legend(loc='upper left', fontsize=20, numpoints=1, handletextpad=0.1, prop={'size':20})
#   plt.xticks([0, 31536000, 63072000], ['2018-01-01', '2019-01-01', '2020-01-01'])
#   plt.xlim([0, 63072000])

#   plt.legend(loc='upper left', fontsize=20, numpoints=1, handletextpad=0.1, prop={'size':20})
#   plt.xticks([0, 31536000, 63072000, 94611600, 126230400], ['2018-01-01', '2019-01-01', '2020-01-01', '2021-01-01', '2022-01-01'])
#   plt.xlim([0, 126230400])

#   plt.legend(loc='upper right', fontsize=20, numpoints=1, handletextpad=0.1)
#   plt.xticks([60393600, 63072000, 65750400, 68256000, 70934400, 73526400, 76204800, 78796800, 81475200, 84153600, 86745600, 89424000],
#      ['2019-12-01', '2020-01-01', '2020-02-01', '2020-03-01', '2020-04-01', '2020-05-01', '2020-06-01', '2020-07-01', '2020-08-01', '2020-09-01', '2020-10-01', '2020-11-01'], rotation=x_rot)
#   plt.xlim([60393600, 89424000])

#   plt.legend(loc='upper right', fontsize=20, numpoints=1, handletextpad=0.1)
#   plt.xticks([60393600, 63072000, 65750400, 68256000, 70934400, 73526400, 76204800, 78796800, 81475200, 84153600, 86745600, 89424000, 92016000, 94694400, 97372800, 99792000, 102470400, 105062400, 107740800, 110332800, 113011200, 115689600, 118281600],
#      ['2019-12-01', '2020-01-01', '2020-02-01', '2020-03-01', '2020-04-01', '2020-05-01', '2020-06-01', '2020-07-01', '2020-08-01', '2020-09-01', '2020-10-01', '2020-11-01', '2020-12-01', '2021-01-01', '2021-02-01', '2021-03-01', '2021-04-01', '2021-05-01', '2021-06-01', '2021-07-01', '2021-08-01', '2021-09-01', '2021-10-01'], rotation=x_rot)
#   plt.xlim([60393600, 118281600])

#   plt.legend(loc='upper right', fontsize=20, numpoints=1, handletextpad=0.1)
#   plt.xticks([60393600, 63072000, 65750400, 68256000, 70934400, 73526400, 76204800, 78796800, 81475200, 84153600, 86745600, 89424000, 92016000, 94694400, 97372800, 99792000, 102470400, 105062400, 107740800, 110332800, 113011200, 115689600, 118281600, 120960000, 123552000, 126230400],
#      ['2019-12-01', '2020-01-01', '2020-02-01', '2020-03-01', '2020-04-01', '2020-05-01', '2020-06-01', '2020-07-01', '2020-08-01', '2020-09-01', '2020-10-01', '2020-11-01', '2020-12-01', '2021-01-01', '2021-02-01', '2021-03-01', '2021-04-01', '2021-05-01', '2021-06-01', '2021-07-01', '2021-08-01', '2021-09-01', '2021-10-01', '2021-11-01', '2021-12-01', '2022-01-01'], rotation=x_rot)
#   plt.xlim([60393600, 126230400])
###   plt.legend(loc='upper right', fontsize=20, numpoints=1, handletextpad=0.1)
###   plt.xticks([0, 86400, 172800, 259200, 345600, 432000, 518400, 604800], ['2022-12-20', '2022-12-21', '2022-12-22', '2022-12-23', '2022-12-24', '2022-12-25', '2022-12-26', '2022-12-27'])
###   plt.xlim([0, 604800])
   plt.legend(loc='upper center', fontsize=20, numpoints=1, handletextpad=0.1)
   plt.xticks([0, 172800, 345600, 518400, 691200, 864000, 1036800, 1209600], ['2022-12-20', '2022-12-22', '2022-12-24', '2022-12-26', '2022-12-28', '2022-12-30', '2023-01-01', '2023-01-03'])
   plt.xlim([0, 1209600])
#   plt.xticks([0, 86400, 172800, 259200], ['2022-12-20', '2022-12-21', '2022-12-22', '2022-12-23'])
#   plt.xlim([0, 259200])
#   plt.xticks([0, 86400], ['2022-12-20', '2022-12-21'])
#   plt.xlim([0, 86400])

   plt.xlabel('Date')
   plt.ylim([-1, 7])
   plt.ylabel('Magnitude')
   plt.tight_layout()
   plt.savefig(out_plot_magvstime_file)


def plot_mag_freq_dist(mag_cat, mag_eqt, mag_missed, out_plot_magfreq_file):
   plt.figure(num=1, figsize=(10,8))
   plt.clf()
#   n, bins, patches = plt.hist([mag_cat, mag_eqt, mag_missed], bins=np.arange(-1, 7.0001, 0.1), label=[str(len(mag_cat))+' detected catalog events',
#      str(len(mag_eqt))+' new EQTransformer events', str(len(mag_missed))+' missed catalog events'], color=['blue', 'cyan', 'red'], edgecolor='black', linewidth=0.5, stacked=True)
   n, bins, patches = plt.hist([mag_missed, mag_cat, mag_eqt], bins=np.arange(-1, 7.0001, 0.1), label=[str(len(mag_missed))+' missed catalog events', str(len(mag_cat))+' detected catalog events', str(len(mag_eqt))+' new EQTransformer events'], color=['red', 'blue', 'cyan'], edgecolor='black', linewidth=0.5, stacked=True)
#   mag_allcat = np.concatenate([mag_cat, mag_missed])
#   n, bins, patches = plt.hist([mag_allcat], bins=np.arange(-1, 7.0001, 0.1), label=[str(len(mag_allcat))+' catalog events'], color=['blue'], edgecolor='black', linewidth=0.5, stacked=True)
#   n, bins, patches = plt.hist([mag_cat, mag_eqt], bins=np.arange(-1, 7.0001, 0.1), label=[str(len(mag_cat))+' detected catalog events', str(len(mag_eqt))+' new EQTransformer events'], color=['blue', 'cyan'], edgecolor='black', linewidth=0.5, stacked=True)
#   n, bins, patches = plt.hist([mag_cat, mag_missed, mag_eqt], bins=np.arange(-1, 7.0001, 0.1), label=[str(len(mag_cat))+' detected catalog events', str(len(mag_missed))+' missed catalog events', str(len(mag_eqt))+' new EQTransformer events'], color=['blue', 'red', 'cyan'], edgecolor='black', linewidth=0.5, stacked=True)
   plt.xlim([-1, 7])
   plt.yscale('log', nonpositive='clip')
#   plt.ylim([0.1, 1000])
   plt.ylim([0.1, 10000])
#   plt.ylim([0.1, 100000])
   plt.xlabel("Magnitude")
   plt.ylabel("Number of events")
   plt.title("Magnitude Distribution")
   plt.legend(prop={'size':18})
   plt.tight_layout()
   plt.savefig(out_plot_magfreq_file)


def plot_missed_mag_dist(mag_missed, out_plot_missedmag_file):
   plt.figure(num=2, figsize=(10,8))
   plt.clf()
   n, bins, patches = plt.hist([mag_missed], bins=np.arange(-1, 7.0001, 0.1), label=[str(len(mag_missed))+' missed catalog events'], color=['red'], edgecolor='black', linewidth=0.5)
   plt.xlim([-1, 7])
#   plt.ylim([0, 60])
   plt.ylim([0, 400])
   plt.xlabel("Magnitude")
   plt.ylabel("Number of events")
   plt.title("Magnitude Distribution - Missed catalog events")
   plt.legend(prop={'size':16})
   plt.tight_layout()
   plt.savefig(out_plot_missedmag_file)



# Plot magnitude vs time, magnitude-frequency distribution

#in_dir = '../Catalog/EQT_20221220_20221227/'
#in_cat_file = in_dir+'EQT_20221220_20221227_REAL_HYPOINVERSE_events_MATCH_magcat.txt'
#in_eqt_new_file = in_dir+'EQT_20221220_20221227_REAL_HYPOINVERSE_events_NEW_magcat.txt'
#in_missed_event_file = in_dir+'EQT_20221220_20221227_REAL_HYPOINVERSE_events_MISSED_magcat.txt'
#out_dir = '../EQT_20221220_20221227/Plots/'
#out_plot_magvstime_all_file = out_dir+'EQT_20221220_20221227_magnitude_vs_time_all_REAL_HYPOINVERSE.pdf'
#out_plot_magvstime_box_file = out_dir+'EQT_20221220_20221227_magnitude_vs_time_box_REAL_HYPOINVERSE.pdf'
#out_plot_magfreq_all_file = out_dir+'EQT_20221220_20221227_magnitude_freq_dist_all_REAL_HYPOINVERSE.pdf'
#out_plot_magfreq_box_file = out_dir+'EQT_20221220_20221227_magnitude_freq_dist_box_REAL_HYPOINVERSE.pdf'

in_dir = '../Catalog/EQT_20221220_20230103/'
in_cat_file = in_dir+'EQT_20221220_20230103_REAL_HYPOINVERSE_events_MATCH_magcat.txt'
in_eqt_new_file = in_dir+'EQT_20221220_20230103_REAL_HYPOINVERSE_events_NEW_magcat.txt'
in_missed_event_file = in_dir+'EQT_20221220_20230103_REAL_HYPOINVERSE_events_MISSED_magcat.txt'
out_dir = '../EQT_20221220_20230103/Plots/'
out_plot_magvstime_all_file = out_dir+'EQT_20221220_20230103_magnitude_vs_time_all_REAL_HYPOINVERSE.pdf'
out_plot_magvstime_box_file = out_dir+'EQT_20221220_20230103_magnitude_vs_time_box_REAL_HYPOINVERSE.pdf'
out_plot_magfreq_all_file = out_dir+'EQT_20221220_20230103_magnitude_freq_dist_all_REAL_HYPOINVERSE.pdf'
out_plot_magfreq_box_file = out_dir+'EQT_20221220_20230103_magnitude_freq_dist_box_REAL_HYPOINVERSE.pdf'

# Boundaries of swarm area box
min_lat=39.5
max_lat=41.5
min_lon=-125
max_lon=-123

# Read in catalog data
[cat_times, cat_lat, cat_lon, cat_depth, cat_mag] = np.loadtxt(in_cat_file, usecols=(0,1,2,3,4), unpack=True)
print("len(cat_times) = ", len(cat_times))

# Read in EQTransformer data
[eqt_times, eqt_lat, eqt_lon, eqt_depth, eqt_mag] = np.loadtxt(in_eqt_new_file, usecols=(0,1,2,3,4), unpack=True)
print("len(eqt_times) = ", len(eqt_times))

# Read in missed catalog data
[missed_times, missed_lat, missed_lon, missed_depth, missed_mag] = np.loadtxt(in_missed_event_file, usecols=(0,1,2,3,4), unpack=True)
print("len(missed_times) = ", len(missed_times))

# Plot only events inside swarm area box - not the entire catalog
[cat_keep_times, cat_keep_mag] = utils_dist.return_events_inside_box(cat_times, cat_mag, cat_lat, cat_lon, min_lat, max_lat, min_lon, max_lon)
[eqt_keep_times, eqt_keep_mag] = utils_dist.return_events_inside_box(eqt_times, eqt_mag, eqt_lat, eqt_lon, min_lat, max_lat, min_lon, max_lon)
[missed_keep_times, missed_keep_mag] = utils_dist.return_events_inside_box(missed_times, missed_mag, missed_lat, missed_lon, min_lat, max_lat, min_lon, max_lon)
print("len(cat_keep_times) = ", len(cat_keep_times))
print("len(eqt_keep_times) = ", len(eqt_keep_times))
print("len(missed_keep_times) = ", len(missed_keep_times))

### Plot magnitude vs time
plot_mag_vs_time(cat_times, eqt_times, missed_times, cat_mag, eqt_mag, missed_mag, out_plot_magvstime_all_file)
plot_mag_vs_time(cat_keep_times, eqt_keep_times, missed_keep_times, cat_keep_mag, eqt_keep_mag, missed_keep_mag, out_plot_magvstime_box_file)

### Plot magnitude-frequency distribution
plot_mag_freq_dist(cat_mag, eqt_mag, missed_mag, out_plot_magfreq_all_file)
plot_mag_freq_dist(cat_keep_mag, eqt_keep_mag, missed_keep_mag, out_plot_magfreq_box_file)
#
#### Plot magnitude distribution of missed events
#plot_missed_mag_dist(missed_mag, out_plot_missedmag_all_file)
#plot_missed_mag_dist(missed_keep_mag, out_plot_missedmag_box_file)

#--------------------
## Plot results for only before or during the sequence
#divide_time = 62805600 # UTCDateTime("2019-12-28T22:00:00")-UTCDateTime("2018-01-01T00:00:00"), in seconds
##ind_cat_selecttime = np.where(cat_keep_times < divide_time) # before sequence
##ind_eqt_selecttime = np.where(eqt_keep_times < divide_time) # before sequence
##ind_missed_selecttime = np.where(missed_keep_times < divide_time) # before sequence
#ind_cat_selecttime = np.where(cat_keep_times > divide_time) # during sequence
#ind_eqt_selecttime = np.where(eqt_keep_times > divide_time) # during sequence
#ind_missed_selecttime = np.where(missed_keep_times > divide_time) # during sequence
#
#cat_keep_selecttime_times = cat_keep_times[ind_cat_selecttime]
#cat_keep_selecttime_mag = cat_keep_mag[ind_cat_selecttime]
#eqt_keep_selecttime_times = eqt_keep_times[ind_eqt_selecttime]
#eqt_keep_selecttime_mag = eqt_keep_mag[ind_eqt_selecttime]
#missed_keep_selecttime_times = missed_keep_times[ind_missed_selecttime]
#missed_keep_selecttime_mag = missed_keep_mag[ind_missed_selecttime]
#print("len(cat_keep_selecttime_times) = ", len(cat_keep_selecttime_times))
#print("len(eqt_keep_selecttime_times) = ", len(eqt_keep_selecttime_times))
#print("len(missed_keep_selecttime_times) = ", len(missed_keep_selecttime_times))
#plot_mag_freq_dist(cat_keep_selecttime_mag, eqt_keep_selecttime_mag, missed_keep_selecttime_mag, out_plot_magfreq_box_selecttime_file)
#plot_mag_vs_time(cat_keep_selecttime_times, eqt_keep_selecttime_times, missed_keep_selecttime_times, cat_keep_selecttime_mag, eqt_keep_selecttime_mag, missed_keep_selecttime_mag, out_plot_magvstime_box_selecttime_file)
#--------------------

