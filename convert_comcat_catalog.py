import datetime

# Convert catalog in USGS ComCat API format to a plain text catalog
#
# 2020-11-02	C. Yoon    First created

####cat_dir = '../Catalog/EQT_20221220_20221227/'
####in_comcat_file = cat_dir+'catalog_ferndale_20221220_20221227_download20221227.txt'
####out_catalog_file = cat_dir+'catalog_new_ferndale_20221220_20221227_download20221227.txt'
#cat_dir = '../Catalog/EQT_20221220_20230103/'
#in_comcat_file = cat_dir+'catalog_ferndale_20221220_20230103_download20230103.txt'
#out_catalog_file = cat_dir+'catalog_new_ferndale_20221220_20230103_download20230103.txt'
##in_comcat_file='../Catalog/EQT_20221220_20221227/catalog_ferndale_20221220_20221227_download20221223.txt'
##out_catalog_file='../Catalog/EQT_20221220_20221227/catalog_new_ferndale_20221220_20221227_download20221223.txt'
#catalog_start_time='2022-12-20T00:00:00'

#cat_dir = '../Catalog/EQT_20221219_20230501/'
#in_comcat_file = cat_dir+'catalog_ferndale_20221219_20230501_download20230505.txt'
#out_catalog_file = cat_dir+'catalog_new_ferndale_20221219_20230501_download20230505.txt'
#catalog_start_time='2022-12-19T00:00:00'

cat_dir = '../Catalog/EQT_20211201_20230601/'
#in_comcat_file = cat_dir+'catalog_ferndale_20211201_20230601_download20230618.txt'
#out_catalog_file = cat_dir+'catalog_new_ferndale_20211201_20230601_download20230618.txt'
in_comcat_file = cat_dir+'catalog_ferndale_20211201_20230601_download20230705.txt'
out_catalog_file = cat_dir+'catalog_new_ferndale_20211201_20230601_download20230705.txt'
catalog_start_time='2021-12-01T00:00:00'

catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
fout = open(out_catalog_file, 'w')
with open(in_comcat_file, 'r') as fcat:
   for line in fcat:
      if (line[0] == '#'): # skip first line
         continue
      split_line = line.split('|')
      origin_time = datetime.datetime.strptime(split_line[1], '%Y-%m-%dT%H:%M:%S.%f')
      time_diff = (origin_time - catalog_ref_time).total_seconds()
      cat_lat = float(split_line[2])
      cat_lon = float(split_line[3])
      cat_depth = float(split_line[4])
      if (split_line[10] == ''): # no magnitude estimate
         print("Missing magnitude estimate, skip: ", split_line)
         continue
      cat_mag = float(split_line[10])
      ev_id = split_line[0].strip('.')
      origin_str = split_line[1].strip()
      # Output plain text catalog with columns: time(sec) since catalog_start_time, latitude, longitude, depth, magnitude, eventid
#      fout.write(('%15.5f %8.5f %8.5f %6.4f %5.3f %s\n') % (time_diff, cat_lat, cat_lon, cat_depth, cat_mag, ev_id))
#      fout.write(('%s %15.5f %8.5f %8.5f %6.4f %5.3f %s\n') % (split_line[1], time_diff, cat_lat, cat_lon, cat_depth, cat_mag, ev_id))
      fout.write(('%s %15.5f %8.5f %8.5f %6.4f %5.3f %s\n') % (origin_str, time_diff, cat_lat, cat_lon, cat_depth, cat_mag, ev_id))
fout.close()
