import datetime

# Write out relocated HYPODD event catalogs for plotting in GMT

catalog_start_time = '2022-12-20T00:00:00'
###in_hypodd_file = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/HYPODD/dtct_hypoDD.reloc'
###out_hypodd_file = '../Catalog/EQT_20221220_20221227/EQT_20221220_20221227_REAL_HYPODD.txt'
in_hypodd_file = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/REAL/HYPODD/dtct_hypoDD.reloc'
out_hypodd_file = '../Catalog/EQT_20221220_20230103/EQT_20221220_20230103_REAL_HYPODD.txt'

catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
fout = open(out_hypodd_file, 'w')
with open(in_hypodd_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      year = int(split_line[10])
      month = int(split_line[11])
      day = int(split_line[12])
      hour = int(split_line[13])
      minute = int(split_line[14])
      second = float(split_line[15])
      origin_time_nosec = datetime.datetime(year, month, day, hour, minute)
      origin_delta = datetime.timedelta(seconds=second)
      origin_time = origin_time_nosec + origin_delta
      num_sec = (origin_time - catalog_ref_time).total_seconds()
      lat_deg = split_line[1]
      lon_deg = split_line[2]
      depth_km = split_line[3]
      mag = split_line[16]
      evid = split_line[0]
      fout.write(("%f %s %s %s %s %s\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid))
fout.close()
