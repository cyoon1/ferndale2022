
#in_phase_file = '../LargeAreaEQTransformer/HYPODD/pr_phases_hypoDD.txt'
#out_event_file = '../LargeAreaEQTransformer/GrowClust/IN/pr_evlist.txt'

base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/'
in_phase_file = base_dir+'HYPODD/mendo_phases_hypoDD.txt' # from ncsn2pha on HYPOINVERSE arc file
out_event_file = base_dir+'GrowClust/IN/mendo_evlist.txt'

fout = open(out_event_file, 'w')
with open(in_phase_file, 'r') as fin:
   for line in fin:
      if (line[0] == '#'):
         fout.write(line[2:])
fout.close()
