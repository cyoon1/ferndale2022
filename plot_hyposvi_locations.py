#dv='cuda:2'
dv='cuda'
# $ gpustat (find which GPUs free)
# $ export CUDA_VISIBLE_DEVICES=5,6,7
# $ echo $CUDA_VISIBLE_DEVICES
# $ CUDA_VISIBLE_DEVICES=5 python plot_hyposvi_locations.py


# EikoNet
from EikoNet import model    as md
from EikoNet import database as db

# HypoSVI
from HypoSVI import location as lc
import pandas as pd
import os

# Plot HypoSVI locations
# Code from Jonny 2020-03-10

#PATH = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/Eiko_out'
#
#xmin               = [-125.0,39.5,-2] #Lat,Long,Depth
#xmax               = [-123.0,41.5,41] #Lat,Long,Depth
#projection         = "+proj=utm +zone=10 +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"
#
#vp_dir = 'VP_MENDO'
#vp_file = vp_dir+'/VP_MENDO.csv'
##vp_model_file = 'Model_Epoch_00012_ValLoss_0.0029753742589006074.pt'
#vp_model_file = 'Model_Epoch_00015_ValLoss_0.006774747649669871.pt'
#vs_dir = 'VS_MENDO'
#vs_file = vs_dir+'/VS_MENDO.csv'
##vs_model_file = 'Model_Epoch_00012_ValLoss_0.003173155303960456.pt'
#vs_model_file = 'Model_Epoch_00015_ValLoss_0.004194190198751657.pt'
#
##in_hyposvi_dir = 'EQT_20221220_20230103_REAL_HYPOINVERSE_PickError010_Events'
##out_catalog_plot_file = 'EQT_20221220_20230103_REAL_HYPOSVI_Catalog.png'
##out_catalog_csv_file = 'EQT_20221220_20230103_REAL_HYPOSVI_Catalog.csv'
#
##in_hyposvi_dir = 'EQT_20221220_20230103_REAL_PickErrorRES_Events'
##out_catalog_plot_file = 'EQT_20221220_20230103_REAL_DIRECTHYPOSVI_Catalog.png'
##out_catalog_csv_file = 'EQT_20221220_20230103_REAL_DIRECTHYPOSVI_Catalog.csv'
#
##in_hyposvi_dir = 'EQT_20221219_20230301_REAL_PickErrorRES_Events'
##out_catalog_plot_file = 'EQT_20221219_20230301_REAL_DIRECTHYPOSVI_Catalog.png'
##out_catalog_csv_file = 'EQT_20221219_20230301_REAL_DIRECTHYPOSVI_Catalog.csv'
#
##in_hyposvi_dir = 'EQT_20230301_20230401_REAL_PickErrorRES_Events'
##out_catalog_plot_file = 'EQT_20230301_20230401_REAL_DIRECTHYPOSVI_Catalog.png'
##out_catalog_csv_file = 'EQT_20230301_20230401_REAL_DIRECTHYPOSVI_Catalog.csv'
#
##in_hyposvi_dir = 'EQT_20230401_20230501_REAL_PickErrorRES_Events'
##out_catalog_plot_file = 'EQT_20230401_20230501_REAL_DIRECTHYPOSVI_Catalog.png'
##out_catalog_csv_file = 'EQT_20230401_20230501_REAL_DIRECTHYPOSVI_Catalog.csv'
#
##in_hyposvi_dir = 'EQT_20221219_20230501_REAL_PickErrorWEIGHT_Events'
##out_catalog_plot_file = 'EQT_20221219_20230501_REAL_DIRECTHYPOSVI_Catalog.png'
##out_catalog_csv_file = 'EQT_20221219_20230501_REAL_DIRECTHYPOSVI_Catalog.csv'
#
#in_hyposvi_dir = 'EQT_20221219_20230501_REAL_MISSED_PickErrorWEIGHT_Events'
#out_catalog_plot_file = 'EQT_20221219_20230501_REAL_MISSED_DIRECTHYPOSVI_Catalog.png'
#out_catalog_csv_file = 'EQT_20221219_20230501_REAL_MISSED_DIRECTHYPOSVI_Catalog.csv'



#----------ENTIRE MTJ REGION---------------
PATH = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/Eiko_out_MTJ'
#xmin               = [-128.0,39.0,-2] #Lat,Long,Depth
#xmax               = [-122.0,44.0,41] #Lat,Long,Depth
xmin               = [-126.0,39.5,-2] #Lat,Long,Depth
xmax               = [-123.0,41.5,41] #Lat,Long,Depth
projection         = "+proj=utm +zone=10 +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"

vp_dir = 'VP_MENDO'
vp_file = vp_dir+'/VP_MENDO.csv'
#vp_model_file = 'Model_Epoch_00008_ValLoss_0.010146594536315678.pt'
vp_model_file = 'Model_Epoch_00015_ValLoss_0.007079711141891049.pt'
vs_dir = 'VS_MENDO'
vs_file = vs_dir+'/VS_MENDO.csv'
#vs_model_file = 'Model_Epoch_00008_ValLoss_0.010756633041384524.pt'
vs_model_file = 'Model_Epoch_00015_ValLoss_0.0050841319948939.pt'

# DETECTED EVENTS
in_event_file = 'EQT_20211201_20230601_REAL_PickErrorWEIGHT.nlloc'
in_hyposvi_dir = 'EQT_20211201_20230601_REAL_PickErrorWEIGHT_Events'
out_catalog_plot_file = 'EQT_20211201_20230601_REAL_DIRECTHYPOSVI_Catalog.png'
out_catalog_csv_file = 'EQT_20211201_20230601_REAL_DIRECTHYPOSVI_Catalog.csv'

## MISSED EVENTS
#in_event_file = 'EQT_20211201_20230601_REAL_MISSED_PickErrorWEIGHT.nlloc'
#in_hyposvi_dir = 'EQT_20211201_20230601_REAL_MISSED_PickErrorWEIGHT_Events'
#out_catalog_plot_file = 'EQT_20211201_20230601_REAL_MISSED_DIRECTHYPOSVI_Catalog.png'
#out_catalog_csv_file = 'EQT_20211201_20230601_REAL_MISSED_DIRECTHYPOSVI_Catalog.csv'


# ------------- VP Velocity Model ----------
vm_vp = db.Graded1DVelocity(('{}/'+vp_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)
model_VP  = md.Model(('{}/'+vp_dir).format(PATH),vm_vp,device=dv)
##model_VP  = md.Model(('{}/'+vp_dir).format(PATH),vm_vp)
model_VP.load(('{}/'+vp_dir+'/'+vp_model_file).format(PATH))

# ------------- VS Velocity Model ----------
vm_vs = db.Graded1DVelocity(('{}/'+vs_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)
model_VS  = md.Model(('{}/'+vs_dir).format(PATH),vm_vs,device=dv)
##model_VS  = md.Model(('{}/'+vs_dir).format(PATH),vm_vs)
model_VS.load(('{}/'+vs_dir+'/'+vs_model_file).format(PATH))


# ------ HypoSVI -------
PATH_EVT = PATH+'/'+in_hyposvi_dir

EVT_org = lc.IO_JSON('{}/Catalogue.json'.format(PATH_EVT),rw_type='r')
keys = []
for key in EVT_org.keys():
   try:
      loc = EVT_org[key]['location']
      keys.append(key)
      continue
   except:
      continue
EVT= { your_key: EVT_org[your_key] for your_key in keys }

LocMethod = lc.HypoSVI([model_VP,model_VS],Phases=['P','S'],device=dv)
LocMethod.plot_info['CataloguePlot']['Minimum Phase Picks']                                           = 8
LocMethod.plot_info['CataloguePlot']['Maximum Location Uncertainty (km)']                             = 30
LocMethod.plot_info['CataloguePlot']['Num Std to define errorbar']                                    = 2
#LocMethod.plot_info['CataloguePlot']['Event Info - [Size, Color, Marker, Alpha]']                     = [0.05,'r','*',0.05]
#LocMethod.plot_info['CataloguePlot']['Event Errorbar - [On/Off(Bool),Linewidth,Color,Alpha]']         = [True,0.1,'r',0.1]
#LocMethod.plot_info['CataloguePlot']['Station Marker - [Size,Color,Names On/Off(Bool)]']              = [150,'b',True]
LocMethod.plot_info['CataloguePlot']['Event Info - [Size, Color, Marker, Alpha]']                     = [0.5,'r','*',0.5]
LocMethod.plot_info['CataloguePlot']['Event Errorbar - [On/Off(Bool),Linewidth,Color,Alpha]']         = [True,0.3,'r',0.3]
LocMethod.plot_info['CataloguePlot']['Station Marker - [Size,Color,Names On/Off(Bool)]']              = [50,'b',True]

#Stations       = pd.read_csv('{}/pr_st.out'.format(PATH),sep=r'\s+')
Stations       = pd.read_csv('{}/mendo_st.out'.format(PATH),sep=r'\s+')
Stations       = Stations.drop_duplicates(['Network','Station'],keep='last').reset_index(drop=True)

LocMethod.CataloguePlot(filepath=(('{}/'+out_catalog_plot_file).format(PATH_EVT)), Events=EVT,
                        user_xmin=[None,None,-1], user_xmax=[None,None,None], Stations=Stations)

LocMethod.Events2CSV(savefile=(('{}/'+out_catalog_csv_file).format(PATH_EVT)))

