from obspy import UTCDateTime
import utils_hypoinverse as utils_hyp

# Read HYPOINVERSE summary output file
# Output file for plotting on GMT map

#-----------------------------------
#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_MATCH_magcat_EQT_20180101_20220101.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_events_MATCH_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_NEW_magcat_EQT_20180101_20220101.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_events_NEW_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_events_MISSED_magcalcml.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/combined_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MATCH_magcat_EQT_20180101_20220101.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_events_MATCH_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_NEW_magcat_EQT_20180101_20220101.txt'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_events_NEW_magcat.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/merged_real_magcatmiss_locate_pr.sum' # catalog magnitudes filled into HYPOINVERSE sum file
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_events_MISSED_magcalcml.txt'

#in_hinv_sum_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/combined_real_magcat_locate_pr.sum'
#catalog_start_time = UTCDateTime('2018-01-01T00:00:00')
#output_plot_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'

#in_hinv_sum_file = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/HYPOINVERSE/merged_real_locate_mendo.sum'
#catalog_start_time = UTCDateTime('2022-12-20T00:00:00')
#output_plot_file = '../Catalog/EQT_20221220_20221227/EQT_20221220_20221227_REAL_HYPOINVERSE_merged_real_locate_mendo.txt'

###catalog_start_time = UTCDateTime('2022-12-20T00:00:00')
###base_in_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/HYPOINVERSE/'
###base_out_dir = '../Catalog/EQT_20221220_20221227/'
####in_hinv_sum_file = base_in_dir+'combined_real_magcat_locate_mendo.sum'
####output_plot_file = base_out_dir+'EQT_20221220_20221227_REAL_HYPOINVERSE_combined_real_magcat_locate_mendo.txt'
####in_hinv_sum_file = base_in_dir+'events_MATCH_magcat_EQT_20221220_20221227.txt'
####output_plot_file = base_out_dir+'EQT_20221220_20221227_REAL_HYPOINVERSE_events_MATCH_magcat.txt'
###in_hinv_sum_file = base_in_dir+'events_NEW_magcat_EQT_20221220_20221227.txt'
###output_plot_file = base_out_dir+'EQT_20221220_20221227_REAL_HYPOINVERSE_events_NEW_magcat.txt'

catalog_start_time = UTCDateTime('2022-12-20T00:00:00')
base_in_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/REAL/HYPOINVERSE/'
base_out_dir = '../Catalog/EQT_20221220_20230103/'
#in_hinv_sum_file = base_in_dir+'combined_real_magcat_locate_mendo.sum'
#output_plot_file = base_out_dir+'EQT_20221220_20230103_REAL_HYPOINVERSE_combined_real_magcat_locate_mendo.txt'
#in_hinv_sum_file = base_in_dir+'events_MATCH_magcat_EQT_20221220_20230103.txt'
#output_plot_file = base_out_dir+'EQT_20221220_20230103_REAL_HYPOINVERSE_events_MATCH_magcat.txt'
in_hinv_sum_file = base_in_dir+'events_NEW_magcat_EQT_20221220_20230103.txt'
output_plot_file = base_out_dir+'EQT_20221220_20230103_REAL_HYPOINVERSE_events_NEW_magcat.txt'

#-----------------------------------

fout = open(output_plot_file, 'w')
with open(in_hinv_sum_file, 'r') as fin:
   for line in fin:
      origin_time = utils_hyp.get_origin_time_hypoinverse_file(line)
      num_sec = origin_time - catalog_start_time

      [lat_deg, lon_deg, depth_km] = utils_hyp.get_lat_lon_depth_hypoinverse_file(line)
      evid = utils_hyp.get_event_id_hypoinverse_file(line)
      mag = 0.01*float(line[147:150])

      fout.write(("%f %f %f %f %f %s\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid))
      print(line)
fout.close()

