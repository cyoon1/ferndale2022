from downloader import downloadMseeds
import os
import sys

# eqt_download_mseed_waveforms.py: script to download MiniSEED waveform data with ObsPy mass downloader
# --> To run in parallel: called by eqt_parallel_download_mseed_waveforms.py

if len(sys.argv) != 4:
   print("Usage: python eqt_download_mseed_waveforms.py <start_date> <end_date> <mseed_dir>")
   print("Usage Example: python eqt_download_mseed_waveforms.py 20200107 20200108 /media/yoon/INT01/PuertoRico/EQT_20200107_20200114/downloads_mseeds/20200107/")
   print("Exiting, not enough arguments...")
   sys.exit(1)

DATE_START = str(sys.argv[1])
DATE_END = str(sys.argv[2])
OUT_MSEED_DIR = str(sys.argv[3])
print("PROCESSING:", DATE_START, DATE_END, OUT_MSEED_DIR)
   

#--------------------------START OF INPUTS------------------------
# Ferndale 2022-12-20
clientlist=["NCEDC", "IRIS"]
minlat=39.5
maxlat=41.5
minlon=-126.0
maxlon=-123.0

# Download these channels, not ALL channels
# Note: "HN[ZNE12]" is not included in default channel priority list, if channel_list=[] input is used
chan_priority_list=["HH[ZNE12]", "BH[ZNE12]", "EH[ZNE12]", "HN[ZNE12]"]

### Time duration is determined by input parameters
tstart=DATE_START+" 00:00:00.00"
tend=DATE_END+" 00:00:00.00"

# Station file (input)
###in_station_json_file='/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/station_list.json'
in_station_json_file='/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/station_list.json'

# Directory to output MiniSEED waveform data
out_data_dir = OUT_MSEED_DIR
if not os.path.exists(out_data_dir):
   os.makedirs(out_data_dir)
#--------------------------END OF INPUTS------------------------

# Download MiniSEED continuous waveform data
downloadMseeds(client_list=clientlist, stations_json=in_station_json_file,
   output_dir=out_data_dir,
   start_time=tstart, end_time=tend, 
   min_lat=minlat, max_lat=maxlat, min_lon=minlon, max_lon=maxlon,
   chunk_size=1, channel_list=chan_priority_list, n_processor=None)
