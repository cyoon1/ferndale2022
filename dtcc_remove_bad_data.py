# dtcc file input: dtcc file
# dtcc file output: dtcc file without bad differential times

base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/'
#in_dtcc_file = base_dir+'HYPODD/EQT_20221220_20221227_mendo_dtcc_net.txt'
#out_dtcc_file = base_dir+'HYPODD/EQT_20221220_20221227_mendo_dtcc_net_maxdt2.txt'
in_dtcc_file = base_dir+'GrowClust/IN/EQT_20221220_20221227_mendo_dtcc.txt'
#out_dtcc_file = base_dir+'GrowClust/IN/EQT_20221220_20221227_mendo_dtcc_maxdt2.txt'
#out_dtcc_file = base_dir+'GrowClust/IN/EQT_20221220_20221227_mendo_dtcc_maxdt5.txt'
#out_dtcc_file = base_dir+'GrowClust/IN/EQT_20221220_20221227_mendo_dtcc_maxdt20.txt'
out_dtcc_file = base_dir+'GrowClust/IN/EQT_20221220_20221227_mendo_dtcc_maxdt30.txt'

#max_dt = 2.0
#max_dt = 5.0
#max_dt = 20.0
max_dt = 30.0

fout = open(out_dtcc_file,'w')
with open(in_dtcc_file,'r') as fin:
   dtcc_list = []
   ev_line = ''
   for line in fin:
      if (line[0] == '#'):
         # From the previous event pair
         if (len(dtcc_list) > 0):
            fout.write(ev_line)
            for dtpair in dtcc_list:
               fout.write(dtpair)
         ev_line = line
         dtcc_list.clear()
      else:
         split_line = line.split()
         val_dt = float(split_line[1])
         if (abs(val_dt) <= max_dt):
            dtcc_list.append(line)

# Last remaining event
if (len(dtcc_list) > 0):
   fout.write(ev_line)
   for dtpair in dtcc_list:
      fout.write(dtpair)

fout.close()
