import json
import sys
import time
import numpy as np
import utils_hypoinverse as utils_hyp
import utils_magnitude as utils_mag

# Compute local magnitude (Wood-Anderson) for EQTransformer events, following magnitude calibration from matching catalog events


def main(EV_IND_FIRST, EV_IND_LAST):

###   base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/'
   base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/'
   hinv_dir = base_dir+'REAL/HYPOINVERSE/'
   in_station_file = base_dir+'station_list_edited.json'
   in_inv_dir = base_dir+'StationXML/'
   in_event_file_dir = base_dir+'downloads_mseeds/'
   in_hinv_arc_file = hinv_dir+'merged_real_locate_mendo.arc' # use the merged file
###   in_calib_file = hinv_dir+'magnitude_calibration_EQT_20221220_20221227.txt'
   in_calib_file = hinv_dir+'magnitude_calibration_EQT_20221220_20230103.txt'

   min_lat = 39.5
   max_lat = 41.5
   min_lon = -125.0
   max_lon = -123.0

   # Get station data
   stations_ = json.load(open(in_station_file))
   # Get station data
   stations_ = json.load(open(in_station_file))

   # Get station inventory for instrument response
   inv_map = utils_mag.get_station_inventory(in_inv_dir, stations_)

   # Get all event data from HYPOINVERSE arc file
   map_events = utils_mag.get_events_map(in_hinv_arc_file, stations_)

   # Get all event phase data from HYPOINVERSE arc file
   [event_dict, ev_id_list] = utils_hyp.get_event_phase_data_hypoinverse_file(in_hinv_arc_file)

   # Distance correction, slope and intercept - get this from matching catalog events
   # Run calibrate_local_magnitude_wood_anderson.py
   #   dist_corr_slope =  0.725917698378 # TEST 20200107-20200114
   #   dist_corr_icpt =  3.76802638352 # TEST 20200107-20200114
   #   dist_corr_slope = 1.38657435857 # PHASETEST 20200107-20200114
   #   dist_corr_icpt = 2.58957042452 # PHASETEST 20200107-20200114
   #dist_corr_slope = 1.41556597881 # NOFILTERPHASETEST 20200107-20200114
   #dist_corr_icpt = 2.49115637769 # NOFILTERPHASETEST 20200107-20200114
   #dist_corr_slope = 1.18039588465 # NOFILTERPHASETEST 20200107-20200114 StationXML
   #dist_corr_icpt = 3.00651850763 # NOFILTERPHASETEST 20200107-20200114 StationXML

   # Distance correction, slope (common) and intercept (per station) - get this from matching catalog events
   order_sta = []
   dist_corr_icpt = []
   with open(in_calib_file, 'r') as fin:
      for line in fin:
         if (line[0] == 'k'):
            dist_corr_slope = float(line.split()[1])
         else:
            split_line = line.split()
            order_sta.append(split_line[0])
            dist_corr_icpt.append(float(split_line[1]))

   # Loop over all events
   t0 = time.time()
   map_return_events = {}
   for kk in range(EV_IND_FIRST, EV_IND_LAST):
      ev_id = ev_id_list[kk]
      median_ML_Richter = utils_mag.calculate_local_magnitude(in_event_file_dir, event_dict, ev_id, order_sta, map_events,
         inv_map, dist_corr_slope, dist_corr_icpt)
      map_return_events[ev_id] = map_events[ev_id]
      map_return_events[ev_id].append(median_ML_Richter) # ML is the last item in the map values
   print("Total runtime magnitude calculation: ", time.time()-t0)

   return map_return_events


if __name__ == "__main__":

   if len(sys.argv) != 3:
      print("Usage: python PARTIALcompute_local_magnitude_wood_anderson.py <start_ev_ind> <end_ev_ind>")
      sys.exit(1)

   EV_IND_FIRST = int(sys.argv[1])
   EV_IND_LAST = int(sys.argv[2])
   print("PROCESSING:", EV_IND_FIRST, EV_IND_LAST)

   main(EV_IND_FIRST, EV_IND_LAST)


