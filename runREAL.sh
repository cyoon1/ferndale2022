#!/bin/bash

# REAL inputs for EQTransformer on Ferndale

if [[ $# -eq 0 ]] ; then
   echo 'Usage, specify start date year-month-day and number of days: ./runREAL.sh <year> <month> <day> <ndays>'
   echo 'Example: ./runREAL.sh 2020 01 07 6'
   exit 1
fi
echo $0, $1, $2, $3

# Check number of CPUs available first
export OMP_NUM_THREADS=84
echo "OMP_NUM_THREADS: " ${OMP_NUM_THREADS}

##R = "0.5/30/0.04/2/5/360/180/17.95/-66.9"; # fast but not as accurate
#R="0.5/30/0.02/2/5/360/180/17.95/-66.9" # slow but more accurate
##R="0.5/30/0.1/4/5/360/180/17.95/-66.9" # TEST only
#G="2.0/30/0.01/1"
#V="6.2/3.3"
#S="4/3/7/1/0.5/0.5/1.5/4.8" # Model2 - reduce false detections
##S="3/1/4/1/0.5/0.5/1.5/4.8" # Model1
##$S = "5/0/18/1/0.5/0.5/1.3/1.8";

# New version of REAL: 2021 July
#R="0.5/40/0.02/2/5/360/180/40.5/-124.0"
#R="0.5/40/0.1/8/5/360/180/40.5/-124.0" #test
R="0.7/40/0.02/2/5/360/180/40.5/-124.3"
#R="0.7/40/0.05/5/5/360/180/40.5/-124.3" # bad locations
G="2.0/40/0.01/1"
V="6.46/3.63"
###S="4/4/8/3/0.5/0.2/1.3/5.0" # 4 P, 4 S, 8 total picks per event
#S="3/3/6/3/0.5/0.2/1.3/5.0" # PRSN minimum: 3 P, 3 S, 6 total picks per event
S="3/3/6/3/0.5/0.2/1.5"
#S="4/4/8/4/0.5/0.2/1.3/5.0" #test
LAT_CENTER=40.5

# Input REAL directory
###realdir="/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL"
realdir="/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/REAL"

station=${realdir}"/mendo_stations.txt"

# Velocity model dependent inputs
ttime=${realdir}"/tt_db/ttdb_mendo.txt"
outeventdir="Events" # directory must already exist

# Start date (hard coded)
#year="2020"
#mon="01"
#day="07"
#D="$year/$mon/$day"
#dir="../Pick/$year$mon$day"

# Start date
year=$1
mon=$2
day=$3
ndays=$4
DATE=$year/$mon/$day
echo "Start date: "$DATE
echo "Number of days: "$ndays

# Loop over days to run REAL
lowerlimit=0
upperlimit=$ndays
#for ii in {0..$ndays}
for ii in $(seq $lowerlimit $upperlimit)
do
#   NEXT_DATE=`gdate -d "$DATE + $ii day" +%Y/%m/%d` #Mac
#   PICK_DATE=`gdate -d "$DATE + $ii day" +%Y%m%d` #Mac
   NEXT_DATE=`date -d "$DATE + $ii day" +%Y/%m/%d` #Linux
   PICK_DATE=`date -d "$DATE + $ii day" +%Y%m%d` #Linux
   D=$NEXT_DATE/$LAT_CENTER
   dir=${realdir}/Pick/$PICK_DATE
   echo $D
   echo $dir
   echo "REAL -D$D -R$R -G$G -S$S -V$V $station $dir $ttime ${realdir}/${outeventdir}/${PICK_DATE}_catalog_sel.txt ${realdir}/${outeventdir}/${PICK_DATE}_phase_sel.txt\n"
   REAL -D$D -R$R -G$G -S$S -V$V $station $dir $ttime ${realdir}/${outeventdir}/${PICK_DATE}_catalog_sel.txt ${realdir}/${outeventdir}/${PICK_DATE}_phase_sel.txt # Run REAL for this day
#   REAL -D$D -R$R -G$G -S$S -V$V $station $dir $ttime # Run REAL for this day
#   mv catalog_sel.txt ${realdir}/Events/${PICK_DATE}_catalog_sel.txt
#   mv phase_sel.txt ${realdir}/Events/${PICK_DATE}_phase_sel.txt
done

#REAL -D$D -R$R -G$G -S$S -V$V $station $dir $ttime
#echo "REAL -D$D -R$R -G$G -S$S -V$V $station $dir $ttime\n"
