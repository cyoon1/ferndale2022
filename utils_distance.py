import numpy as np
from obspy.geodetics.base import gps2dist_azimuth

# Calculate distance between two points or collections of points, including depth
# Returns: distance in km
def distance(lons1, lats1, depths1, lons2, lats2, depths2):
   [dist_m, azAB, azBA] = gps2dist_azimuth(lons1, lats1, lons2, lats2)
   hdist = 0.001*dist_m
   vdist = depths1 - depths2
   return np.sqrt(hdist**2 + vdist**2)


# Return only events inside box
def return_events_inside_box(ev_times, ev_mag, ev_lat, ev_lon, min_lat, max_lat, min_lon, max_lon):
   ev_keep_times = []
   ev_keep_mag = []
   for ic in range(0,len(ev_times)):
      if ((ev_lat[ic] >= min_lat) and (ev_lat[ic] <= max_lat) and (ev_lon[ic] >= min_lon) and (ev_lon[ic] <= max_lon)):
         ev_keep_times.append(ev_times[ic])
         ev_keep_mag.append(ev_mag[ic])
#      else:
#         print(ev_times[ic], ev_mag[ic], ev_lat[ic], ev_lon[ic])
   return [np.asarray(ev_keep_times), np.asarray(ev_keep_mag)]
