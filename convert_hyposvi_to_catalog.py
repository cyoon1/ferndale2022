import datetime
import numpy as np

# Script to convert HypoSVI CSV catalog for GMT plot

#catalog_start_time = '2018-01-01 00:00:00'
#in_hinv_sum_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr.txt'
#in_hsvi_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_Catalog.csv'
#out_hsvi_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_Catalog.txt'
#out_mag_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOSVI_VELZHANG_MagnitudeMap.txt'

catalog_start_time = '2022-12-20 00:00:00'
hsvi_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/Eiko_out/EQT_20221220_20230103_REAL_HYPOINVERSE_PickError010_Events/'
cat_dir = '../Catalog/EQT_20221220_20230103/'
in_hinv_sum_file = cat_dir+'EQT_20221220_20230103_REAL_HYPOINVERSE_combined_real_magcat_locate_mendo.txt'
in_hsvi_file = hsvi_dir+'EQT_20221220_20230103_REAL_HYPOSVI_Catalog.csv'
out_hsvi_file = cat_dir+'EQT_20221220_20230103_REAL_HYPOSVI_Catalog.txt'
out_mag_file = cat_dir+'EQT_20221220_20230103_REAL_HYPOSVI_MagnitudeMap.txt'


[hinv_ot, hinv_lat, hinv_lon, hinv_depth, hinv_mag, hinv_float_evid] = np.loadtxt(in_hinv_sum_file, unpack=True)
hinv_evid = hinv_float_evid.astype(int)
nev_hinv = len(hinv_evid)
ind_not_in_hsvi = np.ones(nev_hinv)

catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%d %H:%M:%S')
nread = 0
nev_hsvi = 0
START_HYPOSVI_ID = 1000000
fout_hsvi = open(out_hsvi_file, 'w')
fout_mag = open(out_mag_file, 'w')
with open(in_hsvi_file, 'r') as fin:
   for line in fin:
      if (line[0] == 'E'): # skip first line
         continue
      split_line = line.strip('\n').split(',')
      origin_time = datetime.datetime.strptime(split_line[1][:-3], '%Y-%m-%d %H:%M:%S.%f')
      num_sec = (origin_time - catalog_ref_time).total_seconds()
      lat_deg = split_line[3]
      lon_deg = split_line[2]
      depth_km = split_line[4]
      hsvi_evid = int(split_line[0]) # hyposvi event id
      ind_hinv = hsvi_evid - START_HYPOSVI_ID # index in hinv arrays corresponding to hsvi_evid
      hinv_curr_evid = hinv_evid[ind_hinv] # hypoinverse event id corresponding to hsvi_evid
      hinv_curr_mag = hinv_mag[ind_hinv] # hypoinverse magnitude corresponding to hsvi_evid
      ind_not_in_hsvi[ind_hinv] = 0
      print(hsvi_evid, ind_hinv, hinv_curr_evid, hinv_curr_mag)

      fout_hsvi.write(("%f %s %s %s %f %s %s %s %s\n") % (num_sec, lat_deg, lon_deg, depth_km, hinv_curr_mag, hsvi_evid, split_line[5], split_line[6], split_line[7]))
      fout_mag.write(("%d %d %d %f\n") % (hsvi_evid, ind_hinv, hinv_curr_evid, hinv_curr_mag))
      nread += 1
fout_hsvi.close()
fout_mag.close()
print("Number of hypoSVI events read in:", nread)
print("Number of hypoInverse events:", nev_hinv)
evid_not_in_hsvi = hinv_evid[np.argwhere(ind_not_in_hsvi)]
print("Number of hypoinverse events not in hypoSVI:", len(evid_not_in_hsvi))
print(evid_not_in_hsvi)
