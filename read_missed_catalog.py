import datetime

# Read in missed catalog events and output in plot-friendly format

#-----------------------------------
#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/events_MISSED_magcat_EQT_20180101_20220101.txt'
#catalog_start_time = '2018-01-01T00:00:00'
#out_missed_event_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_events_MISSED_magcat.txt'

#in_missed_event_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/events_MISSED_magcat_EQT_20180101_20220101.txt'
#catalog_start_time = '2018-01-01T00:00:00'
#out_missed_event_file = '../Catalog/EQT_20180101_20220101/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_events_MISSED_magcat.txt'

catalog_start_time = '2022-12-20T00:00:00'
###in_missed_event_file = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/HYPOINVERSE/events_MISSED_magcat_EQT_20221220_20221227.txt'
###out_missed_event_file = '../Catalog/EQT_20221220_20221227/EQT_20221220_20221227_REAL_HYPOINVERSE_events_MISSED_magcat.txt'
in_missed_event_file = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/REAL/HYPOINVERSE/events_MISSED_magcat_EQT_20221220_20230103.txt'
out_missed_event_file = '../Catalog/EQT_20221220_20230103/EQT_20221220_20230103_REAL_HYPOINVERSE_events_MISSED_magcat.txt'


catalog_ref_time = datetime.datetime.strptime(catalog_start_time, '%Y-%m-%dT%H:%M:%S')
fout = open(out_missed_event_file, 'w')
with open(in_missed_event_file, 'r') as fin:
   for line in fin:
      split_line = line.split()
      origin_time = datetime.datetime.strptime(split_line[0], '%Y-%m-%dT%H:%M:%S.%f')
      num_sec = (origin_time - catalog_ref_time).total_seconds()
      lat_deg = float(split_line[1])
      lon_deg  = float(split_line[2])
      depth_km = float(split_line[3])
      mag = float(split_line[4])
      evid = split_line[5].strip('.')
      fout.write(("%f %f %f %f %f %s\n") % (num_sec, lat_deg, lon_deg, depth_km, mag, evid))
fout.close()
