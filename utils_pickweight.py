# Utilities to convert between:
#  pick weights for hypoinverse (integer: 0, 1, 2, 3, 4)
#  EQT/REAL pick probabilities (between 0.0 and 1.0)
#  pick time uncertainties in seconds (>= 0.0)

# from EQTransformer/utils/associator.py
def PickProb2HypoinverseWeight(pr):
   weight = 4
   if pr > 0.6:
      weight = 0
   elif pr <= 0.6 and pr > 0.5:
      weight = 1
   elif pr <= 0.5 and pr > 0.2:
      weight = 2
   elif pr <= 0.2 and pr > 0.1:
      weight = 3  
   elif pr <= 0.1:
      weight = 4 
   return weight

def HypoinverseWeight2PickError(weight):
   if (weight == 0):
      pickerror = 0.02
   elif (weight == 1):
      pickerror = 0.05
   elif (weight == 2):
      pickerror = 0.1
   elif (weight == 3):
      pickerror = 0.2
   else:
      pickerror = 0.5
   return pickerror

def PickError2HypoinverseWeight(pickerror):
   if (pickerror <= 0.03): # nominal value 0.02
      weight = 0
   elif ((pickerror > 0.03) and (pickerror <= 0.06)): # nominal value 0.05
      weight = 1
   elif ((pickerror > 0.06) and (pickerror <= 0.12)): # nominal value 0.1
      weight = 2
   elif ((pickerror > 0.12) and (pickerror <= 0.24)): # nominal value 0.2
      weight = 3
   else:
      weight = 4
   return weight

