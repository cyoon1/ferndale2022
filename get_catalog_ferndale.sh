#!/bin/bash

# Get catalog with USGS ComCat API
# https://earthquake.usgs.gov/fdsnws/event/1/
#
# 2020-11-02	C. Yoon    First created

# Catalog input/output - times
# Keep track of date when catalog was downloaded, since catalog changes over time (from analyst progress)

###EQT_20221220_20221227
###EQT_20221220_20230103
#t_start='2022-12-19T00:00:00'
##t_start='2022-12-20T00:00:00'
##t_end='2022-12-21T00:00:00'
##t_end='2022-12-23T00:00:00'
####t_end='2022-12-27T00:00:00'
##t_end='2023-01-03T00:00:00'
#t_end='2023-05-01T00:00:00'
##out_file='../Catalog/EQT_20221220_20221227/catalog_ferndale_20221220_20221227_download20221221.txt'
##out_file='../Catalog/EQT_20221220_20221227/catalog_ferndale_20221220_20221227_download20221222.txt'
##out_file='../Catalog/EQT_20221220_20221227/catalog_ferndale_20221220_20221227_download20221223.txt'
####out_file='../Catalog/EQT_20221220_20221227/catalog_ferndale_20221220_20221227_download20221227.txt'
##out_file='../Catalog/EQT_20221220_20230103/catalog_ferndale_20221220_20230103_download20230103.txt'
#out_file='../Catalog/EQT_20221219_20230501/catalog_ferndale_20221219_20230501_download20230505.txt'
##out_file='../Catalog/EQT_20221219_20230501/catalog_ferndale_20221219_20230501_download20230505_nc.txt'
#
## Catalog lat/lon boundaries
#min_lat=39.5
#max_lat=41.5
#min_lon=-125
#max_lon=-123
#out_format='text'

t_start='2021-12-01T00:00:00'
t_end='2023-06-01T00:00:00'
#out_file='../Catalog/EQT_20211201_20230601/catalog_ferndale_20211201_20230601_download20230618.txt'
##out_file='../Catalog/EQT_20211201_20230601/catalog_ferndale_20211201_20230601_download20230618_nc.txt'
#min_lat=39
#max_lat=44
#min_lon=-128
#max_lon=-122
#out_format='text'
out_file='../Catalog/EQT_20211201_20230601/catalog_ferndale_20211201_20230601_download20230705.txt'
#out_file='../Catalog/EQT_20211201_20230601/catalog_ferndale_20211201_20230601_download20230705_nc.txt'
min_lat=39.5
max_lat=41.5
min_lon=-126
max_lon=-123
out_format='text'

#---------------------------------------

# WARNING: different number of events if I use ComCat vs NCEDC catalog
data_src='https://earthquake.usgs.gov/fdsnws/event/1/query'
#data_src='https://service.ncedc.org/fdsnws/event/1/query'
echo ${data_src}

## Minimum magnitude query
#min_mag=5
#out_file='../Catalog/test_catalog_large.txt'
#query_str=${data_src}?format=${out_format}\&minlatitude=${min_lat}\&maxlatitude=${max_lat}\&minlongitude=${min_lon}\&maxlongitude=${max_lon}\&starttime=${t_start}\&endtime=${t_end}\&orderby='time-asc'\&minmagnitude=${min_mag}

# No minimum magnitude query (second line: specific catalog and contributor)
query_str=${data_src}?format=${out_format}\&minlatitude=${min_lat}\&maxlatitude=${max_lat}\&minlongitude=${min_lon}\&maxlongitude=${max_lon}\&starttime=${t_start}\&endtime=${t_end}\&orderby='time-asc'
#query_str=${data_src}?format=${out_format}\&minlatitude=${min_lat}\&maxlatitude=${max_lat}\&minlongitude=${min_lon}\&maxlongitude=${max_lon}\&starttime=${t_start}\&endtime=${t_end}\&catalog=us\&contributor=us\&orderby='time-asc'

# Retrieve catalog from ComCat
echo ${query_str}
wget ${query_str} -O ${out_file}



