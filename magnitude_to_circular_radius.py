import numpy as np

# Assume circular patch with 3 MPa stress drop
# Input magnitude -> get radius of rupture area

def get_circular_radius(mag):
   delta_sigma = 3.0e6
   moment = np.power(10.0, 1.5*(mag + 6.07)) 
   radius = np.power(((7/16.) * (moment / delta_sigma)), 1.0/3.0)
   return radius


###cat_dir = '../Catalog/EQT_20221220_20221227/'
####input_file = cat_dir+'catalog_new_ferndale_20221220_20221227_download20221227.txt'
####input_file = cat_dir+'EQT_20221220_20221227_REAL_HYPOINVERSE_combined_real_magcat_locate_mendo.txt'
####input_file = cat_dir+'EQT_20221220_20221227_REAL_HYPODD.txt'
###input_file = cat_dir+'EQT_20221220_20221227_REAL_GrowClust_maxdt30.txt'

cat_dir = '../Catalog/EQT_20221220_20230103/'
#input_file = cat_dir+'catalog_new_ferndale_20221220_20230103_download20230103.txt'
#input_file = cat_dir+'EQT_20221220_20230103_REAL_HYPOINVERSE_combined_real_magcat_locate_mendo.txt'
input_file = cat_dir+'EQT_20221220_20230103_REAL_HYPOSVI_Catalog.txt'
#input_file = cat_dir+'EQT_20221220_20230103_REAL_HYPODD.txt'

output_file = input_file.replace('.txt', '_diam.txt')
fout = open(output_file, 'w')
with open(input_file, 'r') as fin:
   for line in fin:
      split_line = line.strip('\n').split()
      input_mag = float(split_line[4])
      radius = get_circular_radius(input_mag)
      rad_km = radius/1000.
      print(split_line, radius, rad_km)
      fout.write(('%s %f\n') % (line.strip('\n'), rad_km))
fout.close()
