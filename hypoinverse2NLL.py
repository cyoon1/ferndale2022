import utils_hypoinverse as utils_hyp

# Convert HYPOINVERSE phase input (arc) file format to NLL (NonLinLoc) phase output file

#in_hinv_arc_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/combined_real_magcat_locate_pr.arc'
#out_nll_phase_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/LargeAreaEQTransformer_combined_real_magcat_locate_pr.nlloc'
###out_nll_phase_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/LargeAreaEQTransformer_combined_real_magcat_locate_pr_PickError010.nlloc'
###out_nll_phase_file = '../LargeAreaEQTransformer/20200107_20200114_Model1/REAL/Events/LargeAreaEQTransformer_combined_real_magcat_locate_pr_PickErrorHYPOINVERSE.nlloc'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/combined_real_magcat_locate_pr.arc'
#out_nll_phase_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/FullEQTransformer_combined_real_magcat_locate_pr.nlloc'
###out_nll_phase_file = '/media/yoon/INT01/PuertoRico/FullEQTransformer/20180101_20201101_Model1/REAL/HYPOINVERSE/FullEQTransformer_combined_real_magcat_locate_pr_PickError010.nlloc'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/combined_real_magcat_locate_pr.arc'
#out_nll_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/EQT_20200107_20200114_REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr_PickError010.nlloc'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/combined_real_magcat_locate_pr.arc'
#out_nll_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/EQT_20200107_20200114_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/combined_real_magcat_locate_pr.arc'
#out_nll_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/EQT_20191228_20200114_REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr_PickError010.nlloc'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/combined_real_magcat_locate_pr.arc'
#out_nll_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/EQT_20191228_20200114_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/combined_real_magcat_locate_pr.arc'
#out_nll_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/EQT_20180101_20210601_REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr_PickError010.nlloc'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/combined_real_magcat_locate_pr.arc'
##out_nll_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/EQT_20180101_20210601_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'
#out_nll_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/EQT_20180101_20211001_REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/combined_real_magcat_locate_pr.arc'
#out_nll_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELPRSN_combined_real_magcat_locate_pr_PickError010.nlloc'

#in_hinv_arc_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/combined_real_magcat_locate_pr.arc'
#out_nll_phase_file = '/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/EQT_20180101_20220101_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_PickError010.nlloc'

base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/'
in_hinv_arc_file = base_dir+'REAL/HYPOINVERSE/combined_real_magcat_locate_mendo.arc'
out_nll_phase_file = base_dir+'Eiko_out/EQT_20221220_20230103_REAL_HYPOINVERSE_combined_real_magcat_locate_mendo_PickError010.nlloc'


# Get all event phase data from HYPOINVERSE arc file
[event_dict, ev_id_list] = utils_hyp.get_event_phase_data_hypoinverse_file(in_hinv_arc_file)
nevents = len(event_dict)
print("nevents = ", nevents)

# Output phase data to NLL format file (no event info)
pick_error_sec = 0.1
fout = open(out_nll_phase_file, 'w')
for iev in range(nevents):
   ev_id = ev_id_list[iev]
   curr_ev = event_dict[ev_id]

   for pph in curr_ev['P'].keys():
      sta = pph[2:]
      net = pph[0:2]
      ph = 'P'
      arr_time = curr_ev['P'][pph][1]
      arr_time_second = arr_time.second + 1e-6*arr_time.microsecond
###      pick_error_sec = curr_ev['P'][pph][2]
      fout.write(('%-7s%s   ?    ? %s      ? %4d%02d%02d %02d%02d %7.4f GAU %9.2e -1.00e+00 -1.00e+00 -1.00e+00\n') % (sta, net, ph, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, arr_time_second, pick_error_sec))
#      fout.write(('%-7s%s   ?    ? %s      ? %4d%02d%02d %02d%02d %7.4f GAU  5.00e-02 -1.00e+00 -1.00e+00 -1.00e+00\n') % (sta, net, ph, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, arr_time_second))

   for sph in curr_ev['S'].keys():
      sta = sph[2:]
      net = sph[0:2]
      ph = 'S'
      arr_time = curr_ev['S'][sph][1]
      arr_time_second = arr_time.second + 1e-6*arr_time.microsecond
###      pick_error_sec = curr_ev['P'][pph][2]
      fout.write(('%-7s%s   ?    ? %s      ? %4d%02d%02d %02d%02d %7.4f GAU %9.2e -1.00e+00 -1.00e+00 -1.00e+00\n') % (sta, net, ph, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, arr_time_second, pick_error_sec))
#      fout.write(('%-7s%s   ?    ? %s      ? %4d%02d%02d %02d%02d %7.4f GAU  5.00e-02 -1.00e+00 -1.00e+00 -1.00e+00\n') % (sta, net, ph, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, arr_time_second))

   fout.write('\n')

fout.close()


