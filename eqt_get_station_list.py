from downloader import makeStationList

# eqt_get_station_list.py: script to get list of stations to download data


#--------------------------START OF INPUTS------------------------
clientlist=["NCEDC", "IRIS"]
minlat=39.5
maxlat=41.5
#minlon=-125.0
minlon=-126.0
maxlon=-123.0

# Download these channels, not ALL channels
chan_priority_list=["HH[ZNE12]", "BH[ZNE12]", "EH[ZNE12]", "HN[ZNE12]"]

### Time duration should be for entire time period of interest

tstart="2021-12-01 00:00:00.00"
#tend="2023-05-01 00:00:00.00"
tend="2023-06-01 00:00:00.00"

#tstart="2022-12-19 00:00:00.00"
##tstart="2022-12-20 00:00:00.00"
##tend="2022-12-21 00:00:00.00"
##tend="2022-12-22 00:00:00.00"
####tend="2022-12-27 00:00:00.00"
##tend="2023-01-02 00:00:00.00"
##tend="2023-01-03 00:00:00.00"
##tend="2023-01-10 00:00:00.00"
##tend="2023-02-07 00:00:00.00"
##tend="2023-02-14 00:00:00.00"
##tend="2023-02-21 00:00:00.00"
##tend="2023-03-01 00:00:00.00"
##tend="2023-04-01 00:00:00.00"
#tend="2023-05-01 00:00:00.00"
###out_station_json_file='/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/station_list.json'
out_station_json_file='/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/station_list.json'


#--------------------------END OF INPUTS------------------------

# Get list of stations
makeStationList(json_path=out_station_json_file, client_list=clientlist,
   min_lat=minlat, max_lat=maxlat, min_lon=minlon, max_lon=maxlon,
   start_time=tstart, end_time=tend, 
   channel_list=chan_priority_list,
#   channel_list=[], # don't use this, since it gets ALL channels
   filter_network=["SY"], filter_station=[])
