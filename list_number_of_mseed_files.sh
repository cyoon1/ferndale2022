#!/bin/bash

cd /media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/downloads_mseeds

# List how many station subdirectories in each directory
for i in 2*; do echo -n "$i "; ls "$i" | wc -l; done > number_of_station_directories.txt

# List how many mseed files in each directory
# https://unix.stackexchange.com/questions/34325/sorting-the-output-of-find-print0-by-piping-to-the-sort-command
find . -maxdepth 1 -mindepth 1 -type d -exec sh -c 'echo "{} : $(find "{}" -type f | wc -l)" file\(s\)' \; | sort > number_of_total_mseed_files.txt
find . -maxdepth 2 -mindepth 1 -type d -exec sh -c 'echo "{} : $(find "{}" -type f | wc -l)" file\(s\)' \; | sort > number_of_mseed_files.txt


