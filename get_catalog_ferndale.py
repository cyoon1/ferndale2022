from obspy import read
from obspy import UTCDateTime
from obspy.clients.fdsn import Client
import os

#def main():

# https://docs.obspy.org/packages/obspy.clients.fdsn.html#module-obspy.clients.fdsn
# https://docs.obspy.org/packages/autogen/obspy.clients.fdsn.client.Client.get_events.html#obspy.clients.fdsn.client.Client.get_events
# https://docs.obspy.org/packages/autogen/obspy.core.event.Catalog.write.html#obspy.core.event.Catalog.write

client = Client("USGS")
#client = Client("NCEDC") # phase picks missing
#min_lat = 39.5
#max_lat = 41.5
#min_lon = -125
#max_lon = -123
#
##min_mag = 5
##min_mag = 0
#min_mag = -2
##include_phases=True
#
#t_start = UTCDateTime("2022-12-19T00:00:00")
##t_start = UTCDateTime("2022-12-20T00:00:00")
##t_end = UTCDateTime("2022-12-21T00:00:00")
##out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/CatalogEventsDownload20221222/'
##t_end = UTCDateTime("2022-12-23T00:00:00")
##out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/CatalogEventsDownload20221223/'
####t_end = UTCDateTime("2022-12-27T00:00:00")
####out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/CatalogEventsDownload20221227/'
##t_end = UTCDateTime("2023-01-03T00:00:00")
#t_end = UTCDateTime("2023-05-01T00:00:00")
##out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/CatalogEventsDownload20230103/'
#out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/CatalogEventsDownload20230501/'

#min_lat=39
#max_lat=44
#min_lon=-128
#max_lon=-122
min_lat=39.5
max_lat=41.5
min_lon=-126
max_lon=-123
min_mag = -2
t_start = UTCDateTime("2021-12-01T00:00:00")
t_end = UTCDateTime("2023-06-01T00:00:00")
#out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/CatalogEventsDownload20230618/'
out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/CatalogEventsDownload20230705/'

if not os.path.exists(out_dir):
   os.makedirs(out_dir)

#-------------------------------------
# First, get list of events in catalog

#out_file = '../Catalog/test_puerto_rico_catalog.txt'
#cat = client.get_events(
#   starttime=t_start, endtime=t_end,
#   minlatitude=min_lat, maxlatitude=max_lat,
#   minlongitude=min_lon, maxlongitude=max_lon,
#   orderby='time-asc', filename=out_file)

cat = client.get_events(
   starttime=t_start, endtime=t_end,
   minlatitude=min_lat, maxlatitude=max_lat,
   minlongitude=min_lon, maxlongitude=max_lon,
   orderby='time-asc',
   minmagnitude=min_mag)
print("Number of events in catalog: ", len(cat))

#-------------------------------------
# Next, query each event by its event ID
# This is needed to get catalog phase picks and focal mechanism/moment tensor
# Save the event ID specific info to file (QUAKEML format)
no_event_list = []
for ev in cat:
   ev_id = ev.resource_id.id.split("eventid=")[1].split("&")[0]
#   ev_id = ev.resource_id.id.split('/')[-1]
   try:
      curr_ev = client.get_events(eventid=ev_id)
   except Exception:
      no_event_list.append(ev_id)
      print("WARNING: Unable to get catalog event: ", ev_id)
      continue
   print(curr_ev[0])
   print(curr_ev[0].focal_mechanisms)
   out_ev_file = out_dir+ev_id+'.xml'
   curr_ev[0].write(out_ev_file, format='QUAKEML')

print("Number of Events not downloaded: ", len(no_event_list))
print("Events not downloaded: ", no_event_list)



#if __name__ == "__main__":
#   main()
