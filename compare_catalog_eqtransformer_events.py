from obspy.geodetics.base import gps2dist_azimuth
from collections import defaultdict
import utils_hypoinverse as utils_hyp
import math
import numpy as np


# Read in catalog, ComCat format
def get_comcat_catalog_map(in_catalog_file):
   map_catalog = defaultdict(lambda: defaultdict(int))
   nitem = 0
   with open(in_catalog_file, 'r') as fcat:
      for line in fcat:
         if (line[0] == '#'): # skip first line
            continue
         split_line = line.split('|')
         string_day = split_line[1][0:10].replace('-','') #YYYYMMDD
         hour = split_line[1][11:13]
         minute = split_line[1][14:16]
         second = split_line[1][17:]
         nsec = int(round(3600*int(hour) + 60*int(minute) + float(second))) # number of seconds within the day (0..86400)
#         print(string_day, hour, minute, second, nsec)
         map_catalog[string_day][nsec] = split_line
         nitem += 1
   print("Number of events read in:", nitem)
   return map_catalog


# Script to compare catalog events with EQTransformer events

##in_catalog_file = '../Catalog/EQT_20221220_20221227/catalog_ferndale_20221220_20221221_download20221222.txt' # Comcat text file format - (for magnitudes)
##in_catalog_file = '../Catalog/EQT_20221220_20221227/catalog_ferndale_20221220_20221221_download20221223.txt' # Comcat text file format - (for magnitudes)
#in_catalog_file = '../Catalog/EQT_20221220_20221227/catalog_ferndale_20221220_20221227_download20221227.txt' # Comcat text file format - (for magnitudes)
#out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/HYPOINVERSE/'
#in_hinv_sum_eqt_file = out_dir+'merged_real_locate_mendo.sum' # HYPOINVERSE summary format
#out_match_file = out_dir+'events_MATCH_EQT_20221220_20221227.txt'
#out_missed_file = out_dir+'events_MISSED_EQT_20221220_20221227.txt'
#out_new_file = out_dir+'events_NEW_EQT_20221220_20221227.txt'

####in_catalog_file = '../Catalog/EQT_20221220_20221227/catalog_ferndale_20221220_20221221_download20221222.txt' # Comcat text file format
####in_catalog_file = '../Catalog/EQT_20221220_20221227/catalog_ferndale_20221220_20221221_download20221223.txt' # Comcat text file format
###in_catalog_file = '../Catalog/EQT_20221220_20221227/catalog_ferndale_20221220_20221227_download20221227.txt' # Comcat text file format
###out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/HYPOINVERSE/'
###in_hinv_sum_eqt_file = out_dir+'merged_real_magcat_locate_mendo.sum' # HYPOINVERSE summary format
###out_match_file = out_dir+'events_MATCH_magcat_EQT_20221220_20221227.txt'
###out_missed_file = out_dir+'events_MISSED_magcat_EQT_20221220_20221227.txt'
###out_new_file = out_dir+'events_NEW_magcat_EQT_20221220_20221227.txt'

#in_catalog_file = '../Catalog/EQT_20221220_20230103/catalog_ferndale_20221220_20230103_download20230103.txt' # Comcat text file format - (for magnitudes)
#out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/REAL/HYPOINVERSE/'
#in_hinv_sum_eqt_file = out_dir+'merged_real_locate_mendo.sum' # HYPOINVERSE summary format
#out_match_file = out_dir+'events_MATCH_EQT_20221220_20230103.txt'
#out_missed_file = out_dir+'events_MISSED_EQT_20221220_20230103.txt'
#out_new_file = out_dir+'events_NEW_EQT_20221220_20230103.txt'

in_catalog_file = '../Catalog/EQT_20221220_20230103/catalog_ferndale_20221220_20230103_download20230103.txt' # Comcat text file format
out_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/REAL/HYPOINVERSE/'
in_hinv_sum_eqt_file = out_dir+'merged_real_magcat_locate_mendo.sum' # HYPOINVERSE summary format
out_match_file = out_dir+'events_MATCH_magcat_EQT_20221220_20230103.txt'
out_missed_file = out_dir+'events_MISSED_magcat_EQT_20221220_20230103.txt'
out_new_file = out_dir+'events_NEW_magcat_EQT_20221220_20230103.txt'

#--------------------------


# Get map (dictionary) with catalog event info
# Key is YYYYMMDD for faster search
map_catalog = get_comcat_catalog_map(in_catalog_file)

# Criteria for matching a catalog event with eqtransformer event
#delta_match_sec = 3.0 # seconds
#delta_match_sec = 5.0 # seconds
###delta_match_sec = 10.0 # seconds
delta_match_sec = 15.0 # seconds
#delta_distance_thresh = 10.0 # km
#delta_distance_thresh = 25.0 # km
###delta_distance_thresh = 50.0 # km
delta_distance_thresh = 60.0 # km

# Loop over EQTransformer events in HYPOINVERSE file
diff_cat_arr = []
fmatch_out = open(out_match_file, 'w')
num_match = 0
fnew_out = open(out_new_file, 'w')
num_new = 0
with open(in_hinv_sum_eqt_file, 'r') as fin:
   for line in fin:
      hour = int(line[8:10])
      minute = int(line[10:12])
      second = 0.01*float(line[12:16])
      cur_nsec = 3600*hour + 60*minute + second
      string_day = line[0:8] #YYYYMMDD

      flag_in_catalog = False
      if (string_day in map_catalog):
         cat_day = map_catalog[string_day] # Contains all catalog events with same YYYYMMDD as eqt event

         tmp_dict = defaultdict(int)
         for icat in cat_day:
            diff_cat_det = cur_nsec - icat
            if (abs(diff_cat_det) <= delta_match_sec):
               tmp_dict[icat] = abs(diff_cat_det)

         # Now tmp_dict has all catalog events with origin time within delta_match_sec of eqt event
         if (len(tmp_dict) > 0):
            for imatch in sorted(tmp_dict, key=tmp_dict.get): # traverse in time order
               item_sec = imatch
               print(imatch, map_catalog[string_day][imatch])
               print(line.strip('\n'))
               print("imatch = ", imatch, ", time difference: ", tmp_dict[item_sec])

               cat_lat = float(map_catalog[string_day][item_sec][2])
               cat_lon = float(map_catalog[string_day][item_sec][3])
               cat_depth = float(map_catalog[string_day][item_sec][4])
               cat_mag = float(map_catalog[string_day][item_sec][10])
               print("catalog values: ", cat_lat, cat_lon, cat_depth, cat_mag)

               [eqt_lat, eqt_lon, eqt_depth] = utils_hyp.get_lat_lon_depth_hypoinverse_file(line)
               print("eqtransformer values: ", eqt_lat, eqt_lon, eqt_depth)

               # Check epicentral distance between catalog event and eqt event
               [epi_dist, azAB, azBA] = gps2dist_azimuth(cat_lat, cat_lon, eqt_lat, eqt_lon)
               epi_dist_km = 0.001*epi_dist
               diff_depth_km = cat_depth - eqt_depth
               print("epi_dist_km = ", epi_dist_km, ", diff_depth_km = ", diff_depth_km, "\n")
               
               # If epicentral distance exceeds threshold, do not declare a match
               if (epi_dist_km > delta_distance_thresh):
                  print("WARNING: epi_dist_km above threshold: ", delta_distance_thresh, "\n")
                  continue

               # Found a match between catalog event and eqt event
               flag_in_catalog = True
               num_match += 1
               fmatch_out.write(('%s %s %7.4f %7.4f %5.3f %4.2f %s\n') % (line.strip('\n'), map_catalog[string_day][item_sec][1], cat_lat, cat_lon, cat_depth, cat_mag, map_catalog[string_day][item_sec][0].strip('.')))
               diff_cat_arr.append(cur_nsec-item_sec)
#               print("cur_nsec = ", cur_nsec, ", item_sec = ", item_sec)
               map_catalog[string_day].pop(item_sec) # Remove matching items from catalog map

               if (flag_in_catalog): # only want one matching item with lowest abs(diff_cat_det)
                  break

      # Did not find a match with catalog event; eqt event must be new
      if (not flag_in_catalog): # Detection not found in catalog
         num_new += 1
         fnew_out.write(('%s') % line)

fmatch_out.close()
fnew_out.close()

diff_cat_arr = np.asarray(diff_cat_arr)
print("len(diff_cat_arr) = ", len(diff_cat_arr))
print("min(diff_cat_arr) = ", min(diff_cat_arr))
print("max(diff_cat_arr) = ", max(diff_cat_arr))
print("max(abs(diff_cat_arr)) = ", max(abs(diff_cat_arr)))
#np.savetxt('diff_'+network_str+'.txt', np.sort(diff_cat_arr), fmt='%d')

# Only missed events should remain in catalog map, so write them out
num_missed = 0
fmissed_out = open(out_missed_file, 'w')
for iday in map_catalog:
   for event in map_catalog[iday]:
      num_missed += 1
      # Write: datetime, lat, lon, depth, mag, evid
      fmissed_out.write(('%s %7.4f %7.4f %5.3f %4.2f %s\n') % (map_catalog[iday][event][1], float(map_catalog[iday][event][2]), float(map_catalog[iday][event][3]), float(map_catalog[iday][event][4]), float(map_catalog[iday][event][10]), map_catalog[iday][event][0].strip('.')))
fmissed_out.close()

print("Number of match catalog events: ", num_match)
print("Number of new events not in catalog: ", num_new)
print("Number of missed catalog events: ", num_missed)

