# dtcc file input: network+station code (for HYPODD input from HYPOINVERSE)
# dtcc file output: only station code (max 5 characters for GrowClust input)

base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/'
in_dtcc_file = base_dir+'HYPODD/EQT_20221220_20221227_mendo_dtcc_net.txt'
out_dtcc_file = base_dir+'GrowClust/IN/EQT_20221220_20221227_mendo_dtcc.txt'

fout = open(out_dtcc_file,'w')
with open(in_dtcc_file,'r') as fin:
   for line in fin:
      if (line[0] == '#'):
         fout.write(line)
      else:
         fout.write(line[2:]) # skip the first 2 letters (network code)
fout.close()
