import json
import os
from obspy import UTCDateTime
from obspy.clients.fdsn import Client


def main():

###   base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/'
   base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/'
   in_sta_file = base_dir+'station_list_edited.json'
   out_base_dir = base_dir+'StationXML/'
   start_time = UTCDateTime("2021-12-01T00:00:00")
   end_time = UTCDateTime("2023-06-01T00:00:00")
   client = Client("NCEDC")
#   client = Client("IRIS")

   if not os.path.exists(out_base_dir):
      os.makedirs(out_base_dir)

   json_file = open(in_sta_file)
   stations_ = json.load(json_file)

   for sta,val in stations_.items():
      net = val['network']
      out_stationxml_dir = out_base_dir+sta+'/'
      if not os.path.exists(out_stationxml_dir):
         os.makedirs(out_stationxml_dir)
      out_stationxml_file = out_stationxml_dir+net+'.'+sta+'.xml'
      inv = client.get_stations(network=net, station=sta, starttime=start_time, endtime=end_time, level='response', filename=out_stationxml_file, format='xml')


if __name__ == "__main__":
   main()
