import datetime
import utils_hypoinverse as utils_hyp
# Convert REAL phase output file to HYPOINVERSE phase input file

# from EQTransformer/utils/associator.py
def _weighcalculator_prob(pr):
    'calculate the picks weights'
    weight = 4
    if pr > 0.6:
        weight = 0
    elif pr <= 0.6 and pr > 0.5:
        weight = 1
    elif pr <= 0.5 and pr > 0.2:
        weight = 2
    elif pr <= 0.2 and pr > 0.1:
        weight = 3  
    elif pr <= 0.1:
        weight = 4 
    return weight


###base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/'
###in_real_phase_file = base_dir+'REAL/Events/EQT_20221220_20221227_phase_sel.txt'
###out_hinv_phase_file = base_dir+'REAL/HYPOINVERSE/EQT_20221220_20221227.phs'
base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/'
in_real_phase_file = base_dir+'REAL/Events/EQT_20221220_20230103_phase_sel.txt'
out_hinv_phase_file = base_dir+'REAL/HYPOINVERSE/EQT_20221220_20230103.phs'

ev_id = 200001 # EQTransformer convention for event id
fout = open(out_hinv_phase_file, 'w')
with open(in_real_phase_file, 'r') as freal:
   for line in freal:
      split_line = line.split()
      if (split_line[0].isnumeric()): # event line
         year = int(split_line[1])
         month = int(split_line[2])
         day = int(split_line[3])
         hour = int(split_line[4][0:2])
         minute = int(split_line[4][3:5])
         origin_time_nosec = datetime.datetime(year, month, day, hour, minute)
         origin_delta = datetime.timedelta(seconds=float(split_line[4][6:12])) # might be negative
         origin_time = origin_time_nosec + origin_delta
         origin_time_second = utils_hyp.get_rounded_seconds(origin_time)
         [lat_int, lat_char, lat_min] = utils_hyp.output_lat_hypoinverse_format(split_line[7])
         [lon_int, lon_char, lon_min] = utils_hyp.output_lon_hypoinverse_format(split_line[8])
         depth = split_line[9]
         depth_out = round(100*float(depth))
         nphases = int(split_line[14])
         count_phases = 0
         fout.write(('%4d%02d%02d%02d%02d%04d%2d%1s%4s%3d%1s%4s%5d%3d\n') % (origin_time.year, origin_time.month, origin_time.day, origin_time.hour, origin_time.minute, origin_time_second, lat_int, lat_char, lat_min, lon_int, lon_char, lon_min, depth_out, 0))
#         print(ev_id, year, month, day, hour, minute, second, lat, lat_int, lat_char, dlat, lat_min, lon, lon_int, lon_char, dlon, lon_min, depth_out, nphases, origin_time_str, origin_time)
         print(split_line)
      else: # phase line
         net = split_line[0]
         sta = "{:<5}".format(split_line[1])
         ph = split_line[2]
         tt = float(split_line[4])
         delta = datetime.timedelta(seconds=tt)
         arr_time = origin_time + delta
         res = float(split_line[6])
         weight = float(split_line[7])
         [chan, p_remark, s_remark, p_res, s_res, p_weight, s_weight, p_arr_time_sec, s_arr_time_sec] = utils_hyp.output_phase_hypoinverse_format(
            ph, arr_time, fm_p='I', fm_s='E', res=res, weight=_weighcalculator_prob(weight))
         count_phases += 1
         fout.write(('%5s%2s  %3s %2s %1d%4d%02d%02d%02d%02d%5d%4s   %5d%2s %1d%4s\n') % (sta, net, chan, p_remark, p_weight, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, p_arr_time_sec, p_res, s_arr_time_sec, s_remark, s_weight, s_res))
#         print(net, sta, ph, tt, arr_time, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, utils_hyp.get_rounded_seconds(arr_time), res, p_res, s_res, weight, p_weight, s_weight)
#         print(split_line)
      if (count_phases == nphases):
         fout.write("{:<62}".format(' ')+"%10d"%(ev_id)+'\n');
         ev_id += 1
fout.close()



