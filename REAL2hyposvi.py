import datetime
import utils_pickweight as utils_pickwt
# Convert REAL phase output file to HYPOSVI input file


####base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/'
####in_real_phase_file = base_dir+'REAL/Events/EQT_20221220_20221227_phase_sel.txt'
####out_hinv_phase_file = base_dir+'REAL/HYPOINVERSE/EQT_20221220_20221227.phs'
#base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/'
#in_real_phase_file = base_dir+'REAL/Events/EQT_20221220_20230103_phase_sel.txt'
#out_hinv_phase_file = base_dir+'REAL/DIRECTHYPOSVI/EQT_20221220_20230103_REAL_PickErrorRES.nlloc'
##out_hinv_phase_file = base_dir+'REAL/HYPOINVERSE/EQT_20221220_20230103.phs'

base_dir = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/'
#in_real_phase_file = base_dir+'REAL/Events/EQT_20221219_20230301_phase_sel.txt'
#out_hinv_phase_file = base_dir+'Eiko_out/EQT_20221219_20230301_REAL_PickErrorRES.nlloc'
#in_real_phase_file = base_dir+'REAL/Events/EQT_20230301_20230401_phase_sel.txt'
#out_hinv_phase_file = base_dir+'Eiko_out/EQT_20230301_20230401_REAL_PickErrorRES.nlloc'
#in_real_phase_file = base_dir+'REAL/Events/EQT_20230401_20230501_phase_sel.txt'
#out_hinv_phase_file = base_dir+'Eiko_out/EQT_20230401_20230501_REAL_PickErrorRES.nlloc'
#in_real_phase_file = base_dir+'REAL/Events/EQT_20221219_20230501_phase_sel.txt'
#out_hinv_phase_file = base_dir+'Eiko_out/EQT_20221219_20230501_REAL_PickErrorWEIGHT.nlloc'
in_real_phase_file = base_dir+'REAL/Events/EQT_20211201_20230601_phase_sel.txt'
out_hinv_phase_file = base_dir+'Eiko_out_MTJ/EQT_20211201_20230601_REAL_PickErrorWEIGHT.nlloc'

#ev_id = 200001 # EQTransformer convention for event id
#pick_error_sec = 0.1
# Output phase data to NLL format file (no event info)
num_events = 0
fout = open(out_hinv_phase_file, 'w')
with open(in_real_phase_file, 'r') as freal:
   for line in freal:
      split_line = line.split()
      if (split_line[0].isnumeric()): # event line
         year = int(split_line[1])
         month = int(split_line[2])
         day = int(split_line[3])
         hour = int(split_line[4][0:2])
         minute = int(split_line[4][3:5])
         origin_time_nosec = datetime.datetime(year, month, day, hour, minute)
         origin_delta = datetime.timedelta(seconds=float(split_line[4][6:12])) # might be negative
         origin_time = origin_time_nosec + origin_delta
         nphases = int(split_line[14])
         count_phases = 0
         num_events += 1
         print(split_line) # do not write out anything to file
      else: # phase line
         net = split_line[0]
         sta = "{:<5}".format(split_line[1])
         ph = split_line[2]
         tt = float(split_line[4])
         delta = datetime.timedelta(seconds=tt)
         arr_time = origin_time + delta
         arr_time_second = arr_time.second + 1e-6*arr_time.microsecond
###         res = abs(float(split_line[6])) # pick error in seconds "ErrMag"
         hypoinverse_weight = utils_pickwt.PickProb2HypoinverseWeight(float(split_line[7])) # hypoinverse weight (integer from 0 to 4) based on pick probability
         pick_error_sec = utils_pickwt.HypoinverseWeight2PickError(hypoinverse_weight) # pick error (seconds) based on hypoinverse weight 
         count_phases += 1
###         fout.write(('%-7s%s   ?    ? %s      ? %4d%02d%02d %02d%02d %7.4f GAU %9.2e -1.00e+00 -1.00e+00 -1.00e+00\n') % (sta, net, ph, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, arr_time_second, res))
         fout.write(('%-7s%s   ?    ? %s      ? %4d%02d%02d %02d%02d %7.4f GAU %9.2e -1.00e+00 -1.00e+00 -1.00e+00\n') % (sta, net, ph, arr_time.year, arr_time.month, arr_time.day, arr_time.hour, arr_time.minute, arr_time_second, pick_error_sec))
      if (count_phases == nphases):
         fout.write('\n')
#         ev_id += 1
fout.close()
print("Number of events in HYPOSVI input file: ", num_events)
