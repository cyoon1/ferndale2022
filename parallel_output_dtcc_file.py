import numpy as np
import subprocess
import multiprocessing
import PARTIALoutput_dtcc_file as dtcc


def get_list_of_block_indices(i_start, i_end, nblock_i, j_start, j_end, nblock_j):
   block_indices = []
   for iblock in range(nblock_i):
      i_now_start = i_start[iblock]
      i_now_end = i_end[iblock]
      print("\n # i_now_start = ", i_now_start, ", i_now_end = ", i_now_end)

      for jblock in range(nblock_j):
         if (jblock < iblock): # avoid duplicates
            continue

         j_now_start = j_start[jblock]
         j_now_end = j_end[jblock]
         print("j_now_start = ", j_now_start, ", j_now_end = ", j_now_end)
         block_indices.append([i_now_start, i_now_end, j_now_start, j_now_end])
   return block_indices


def run_dtcc(block_index):
   print("block_index = ", block_index)
   process = subprocess.Popen(('python PARTIALoutput_dtcc_file.py %s %s %s %s' % (block_index[0], block_index[1], block_index[2], block_index[3])), stdout=subprocess.PIPE, shell=True)
   output, error = process.communicate()
   print(output.decode('UTF-8').strip())
#   dtcc.main(block_index[0], block_index[1], block_index[2], block_index[3]) # This also works, but stdout output is mixed up
#   dtcc.main(i_now_start, i_now_end, j_now_start, j_now_end)


# i: number of rows, j: number of columns
#num_i = 30
#num_i = 636
#num_i = 599
#num_i = 1404

# EQT_20221220_20221227
#num_i = 1722

# EQT_20221220_20230103
num_i = 1916

num_j = num_i

# number of parallel blocks in each dimension
#nblock_i = 4
#nblock_i = 2
nblock_i = 1
nblock_j = nblock_i

# number of elements per block
blocksize_i = int(num_i) // int(nblock_i)
blocksize_j = int(num_j) // int(nblock_j)
remain_i = int(num_i) % int(nblock_i)
remain_j = int(num_j) % int(nblock_j)

print("num_i = ", num_i, ", num_j = ", num_j)
print("nblock_i = ", nblock_i, ", nblock_j = ", nblock_j)
print("blocksize_i = ", blocksize_i, ", blocksize_j = ", blocksize_j)
print("remain_i = ", remain_i, ", remain_j = ", remain_j)

# starting and ending indices for each block
i_start = blocksize_i*np.arange(0,nblock_i)
i_end = i_start + blocksize_i
i_end[-1] += remain_i
j_start = blocksize_j*np.arange(0,nblock_j)
j_end = j_start + blocksize_j
j_end[-1] += remain_j
print("i_start = ", i_start, ", i_end = ", i_end)
print("j_start = ", j_start, ", j_end = ", j_end)


#### Parallel multi-block calculation
block_indices = get_list_of_block_indices(i_start, i_end, nblock_i, j_start, j_end, nblock_j)
print("len(block_indices) = ", len(block_indices))

#num_cores = multiprocessing.cpu_count()
num_cores = 1
print("num_cores = ", num_cores)

pool = multiprocessing.Pool(processes=num_cores)
pool.map(run_dtcc, block_indices)


#### Serial multi-block test
#num_blocks = 0
#for iblock in range(nblock_i):
#   i_now_start = i_start[iblock]
#   i_now_end = i_end[iblock]
#   print("\n # i_now_start = ", i_now_start, ", i_now_end = ", i_now_end)
#
#   for jblock in range(nblock_j):
#      if (jblock < iblock): # avoid duplicates
#         continue
#
#      j_now_start = j_start[jblock]
#      j_now_end = j_end[jblock]
#      print("j_now_start = ", j_now_start, ", j_now_end = ", j_now_end)
#
#      num_blocks += 1
#
#      # Compute dtcc for this block
#      dtcc.main(i_now_start, i_now_end, j_now_start, j_now_end)
#print("num_blocks = ", num_blocks)

