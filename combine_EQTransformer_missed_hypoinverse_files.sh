#!/bin/bash

# combine_EQTransformer_missed_hypoinverse_files.sh
# combine hypoinverse arc/sum files from EQTransformer and missed catalog events (should already have magnitudes)

#event_dir=/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELPRSN/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20200107_20200114/REAL/HYPOINVERSE_VELZHANG/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELPRSN/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20191228_20200114/REAL/HYPOINVERSE_VELZHANG/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELPRSN/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20210601/REAL/HYPOINVERSE_VELZHANG/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELPRSN/
#event_dir=/media/yoon/INT01/PuertoRico/EQT_20180101_20220101/3REAL/HYPOINVERSE_VELZHANG/
###event_dir=/media/yoon/INT01/Ferndale2022/EQT_20221220_20221227/REAL/HYPOINVERSE/
event_dir=/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/REAL/HYPOINVERSE/

cd ${event_dir}

#cat merged_real_magcat_locate_pr.arc merged_real_magcatmiss_locate_pr.arc > combined_real_magcat_locate_pr.arc
#cat merged_real_magcat_locate_pr.sum merged_real_magcatmiss_locate_pr.sum > combined_real_magcat_locate_pr.sum

cat merged_real_magcat_locate_mendo.arc merged_real_magcatmiss_locate_mendo.arc > combined_real_magcat_locate_mendo.arc
cat merged_real_magcat_locate_mendo.sum merged_real_magcatmiss_locate_mendo.sum > combined_real_magcat_locate_mendo.sum

