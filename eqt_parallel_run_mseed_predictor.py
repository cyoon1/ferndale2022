import datetime
from dateutil.relativedelta import relativedelta
import os
import time
import subprocess
import multiprocessing

# eqt_parallel_run_mseed_predictor.py: script to run EQTransformer detector/picker on MiniSEED waveform data
# --> Calls eqt_run_mseed_predictor.py

def get_list_of_days_det(day_start, day_end, in_base_mseed_dir, out_base_det_dir, delta, folder_format):
   start_date = datetime.datetime.strptime(day_start, "%Y%m%d")
   end_date = datetime.datetime.strptime(day_end, "%Y%m%d")
   date_inputs = []
   # Loop over days
   t0 = time.time()
   while (start_date < end_date):
      # Create directory for start_date day if it doesn't already exist
      in_mseed_dir = in_base_mseed_dir+datetime.datetime.strftime(start_date, folder_format)+'/'
      out_det_dir = out_base_det_dir+datetime.datetime.strftime(start_date, folder_format)+'/'
      next_date = start_date + delta
      date_inputs.append([in_mseed_dir, out_det_dir])
      start_date = next_date
   return date_inputs 


def run_mseed_predictor_script(date_inputs):
   print("date_inputs = ", date_inputs)

   print('python eqt_run_mseed_predictor.py %s %s' % (date_inputs[0], date_inputs[1]))
   process = subprocess.Popen(('python eqt_run_mseed_predictor.py %s %s' % (date_inputs[0], date_inputs[1])), stdout=subprocess.PIPE, shell=True)

   output, error = process.communicate()
   print(output.decode('UTF-8').strip())



#--------------------------START OF INPUTS------------------------

# EQT_20211201_20230601
day_start="20211201"
#day_start="20221219"
##day_end="20230207"
##day_start="20230207"
##day_end="20230214"
##day_start="20230214"
##day_end="20230221"
##day_start="20230221"
##day_end="20230301"
##day_start="20230228"
##day_end="20230401"
##day_start="20230401"
#day_end="20230501"
#day_start="20230501"
day_end="20230601"
base_dir = "/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/"
in_base_mseed_dir = base_dir+"zeropad_mseeds/"
out_base_det_dir = base_dir+"mseed_detections/"
delta = datetime.timedelta(days=1) # one sub-directory per day
folder_format = "%Y%m%d"


#--------------------------END OF INPUTS------------------------

if not os.path.exists(out_base_det_dir):
   os.makedirs(out_base_det_dir)

# Get input parameters for each parallel run: one folder per day
date_inputs = get_list_of_days_det(day_start, day_end, in_base_mseed_dir, out_base_det_dir, delta, folder_format)
print("len(date_inputs) = ", len(date_inputs))
print("date_inputs = ", date_inputs)

#num_cores = multiprocessing.cpu_count()
#num_cores = 1
#num_cores = 8 # Might need to limit this, based on memory constraints
#num_cores = 24
#num_cores = 32
num_cores = 48
#num_cores = 84
print("num_cores = ", num_cores)
# Run EQTransformer detector/picker in parallel: eqt_parallel_run_mseed_predictor.py
pool = multiprocessing.Pool(processes=num_cores)
pool.map(run_mseed_predictor_script, date_inputs)

