#!/bin/bash

# Plot Puerto Rico earthquakes on a map

#gmt gmtset BASEMAP_TYPE plain
#gmt gmtset HEADER_FONT_SIZE 24p
#gmt gmtset LABEL_FONT_SIZE 16p # smaller font for labels
##gmt gmtset PLOT_DEGREE_FORMAT +DF
#gmt gmtset PLOT_DEGREE_FORMAT -DF
#gmt gmtset GRID_PEN_PRIMARY 0.1p
#
gmt gmtset MAP_FRAME_TYPE plain     # map outline - single line with ticks
gmt gmtset FORMAT_GEO_MAP DD        # labels on map - decimal degrees

gmt gmtset LABEL_FONT_SIZE 20p
gmt gmtset FONT_ANNOT 20p
#gmt gmtset LABEL_FONT_SIZE 32p #poster
#gmt gmtset FONT_ANNOT 32p #poster

###in_cat_dir=../Catalog/EQT_20221220_20221227
###out_plot_dir=../EQT_20221220_20221227/Plots

in_cat_dir=../Catalog/EQT_20221220_20230103
out_plot_dir=../EQT_20221220_20230103/Plots

out_color_filename=depthcolors.cpt
out_color_file=${out_plot_dir}/${out_color_filename}
out_color_time_filename=timecolors.cpt
out_color_time_file=${out_plot_dir}/${out_color_time_filename}

in_fault_dir=/Users/cyoon/Documents/FaultFiles/Qfaults_GIS/SHP/
in_qfault_file=${in_fault_dir}/Qfaults_US_Database.gmt
in_offshore_fault_file=${in_fault_dir}/ca_offshore.gmt
in_fault_file=${in_cat_dir}/ca_faults.gmt

in_city_file=${in_cat_dir}/humboldt_cities.txt
###in_mt_file=${in_cat_dir}/EQT_20221220_20221227_mendo_moment_tensor_display.txt
in_mt_file=${in_cat_dir}/EQT_20221220_20230103_mendo_moment_tensor_display.txt

###in_cat_file=${in_cat_dir}/catalog_new_ferndale_20221220_20221227_download20221227_diam.txt
###out_map_eqt_hinv_depth_file=${out_plot_dir}/catalog_new_ferndale_20221220_20221227_depth
###out_map_eqt_hinv_time_file=${out_plot_dir}/catalog_new_ferndale_20221220_20221227_time
###
####in_cat_file=${in_cat_dir}/EQT_20221220_20221227_REAL_HYPOINVERSE_combined_real_magcat_locate_mendo_diam.txt
####out_map_eqt_hinv_depth_file=${out_plot_dir}/EQT_20221220_20221227_REAL_HYPOINVERSE_combined_depth
####out_map_eqt_hinv_time_file=${out_plot_dir}/EQT_20221220_20221227_REAL_HYPOINVERSE_combined_time
###
####in_cat_file=${in_cat_dir}/EQT_20221220_20221227_REAL_GrowClust_maxdt30_diam.txt
####out_map_eqt_hinv_depth_file=${out_plot_dir}/EQT_20221220_20221227_REAL_GrowClust_maxdt30_depth
####out_map_eqt_hinv_time_file=${out_plot_dir}/EQT_20221220_20221227_REAL_GrowClust_maxdt30_time
###
####in_cat_file=${in_cat_dir}/EQT_20221220_20221227_REAL_HYPODD_diam.txt
####out_map_eqt_hinv_depth_file=${out_plot_dir}/EQT_20221220_20221227_REAL_HYPODD_depth
####out_map_eqt_hinv_time_file=${out_plot_dir}/EQT_20221220_20221227_REAL_HYPODD_time

#in_cat_file=${in_cat_dir}/catalog_new_ferndale_20221220_20230103_download20230103_diam.txt
#out_map_eqt_hinv_depth_file=${out_plot_dir}/catalog_new_ferndale_20221220_20230103_depth
#out_map_eqt_hinv_time_file=${out_plot_dir}/catalog_new_ferndale_20221220_20230103_time

#in_cat_file=${in_cat_dir}/EQT_20221220_20230103_REAL_HYPOINVERSE_combined_real_magcat_locate_mendo_diam.txt
#out_map_eqt_hinv_depth_file=${out_plot_dir}/EQT_20221220_20230103_REAL_HYPOINVERSE_combined_depth
#out_map_eqt_hinv_time_file=${out_plot_dir}/EQT_20221220_20230103_REAL_HYPOINVERSE_combined_time

#in_cat_file=${in_cat_dir}/EQT_20221220_20230103_REAL_HYPOSVI_combined_real_magcat_locate_mendo_diam.txt
#out_map_eqt_hinv_depth_file=${out_plot_dir}/EQT_20221220_20230103_REAL_HYPOSVI_combined_depth
#out_map_eqt_hinv_time_file=${out_plot_dir}/EQT_20221220_20230103_REAL_HYPOSVI_combined_time

#in_cat_file=${in_cat_dir}/EQT_20221220_20230103_REAL_HYPOSVI_Catalog_diam.txt
#out_map_eqt_hinv_depth_file=${out_plot_dir}/EQT_20221220_20230103_REAL_HYPOSVI_Catalog_depth
#out_map_eqt_hinv_time_file=${out_plot_dir}/EQT_20221220_20230103_REAL_HYPOSVI_Catalog_time

#in_cat_file=${in_cat_dir}/EQT_20221220_20230103_REAL_GrowClust_maxdt30_diam.txt
#out_map_eqt_hinv_depth_file=${out_plot_dir}/EQT_20221220_20230103_REAL_GrowClust_maxdt30_depth
#out_map_eqt_hinv_time_file=${out_plot_dir}/EQT_20221220_20230103_REAL_GrowClust_maxdt30_time

#in_cat_file=${in_cat_dir}/EQT_20221220_20230103_REAL_GrowClust_diam.txt
#out_map_eqt_hinv_depth_file=${out_plot_dir}/EQT_20221220_20230103_REAL_GrowClust_depth
#out_map_eqt_hinv_time_file=${out_plot_dir}/EQT_20221220_20230103_REAL_GrowClust_time

#in_cat_file=${in_cat_dir}/20221220_20230103_ORIG_HYPODD/EQT_20221220_20230103_REAL_HYPODD_diam.txt
#out_map_eqt_hinv_depth_file=${out_plot_dir}/20221220_20230103_ORIG_HYPODD/EQT_20221220_20230103_REAL_HYPODD_depth
#out_map_eqt_hinv_time_file=${out_plot_dir}/20221220_20230103_ORIG_HYPODD/EQT_20221220_20230103_REAL_HYPODD_time

in_cat_file=${in_cat_dir}/EQT_20221220_20230103_REAL_HYPODD_diam.txt
out_map_eqt_hinv_depth_file=${out_plot_dir}/EQT_20221220_20230103_REAL_HYPODD_depth
out_map_eqt_hinv_time_file=${out_plot_dir}/EQT_20221220_20230103_REAL_HYPODD_time

#in_eqt_hinv_file=${in_cat_dir}/EQT_20221220_20221227_3REAL_HYPOINVERSE_VELZHANG_combined_real_magcat_locate_pr_diam.txt
#in_eqt_hinv_file=${in_cat_dir}/EQT_20221220_20221227_3REAL_HYPOINVERSE_VELZHANG_events_MISSED_magcat_diam.txt
#out_map_eqt_hinv_depth_file=${out_plot_dir}/EQT_20221220_20221227_3REAL_HYPOINVERSE_VELZHANG_combined_depth_noxsec
#out_map_eqt_hinv_depth_file=${out_plot_dir}/EQT_20221220_20221227_3REAL_HYPOINVERSE_VELZHANG_combined_depth_MISSED_noxsec

###in_sta_file=${in_cat_dir}/EQT_20221220_20221227_mendo_stations.txt
in_sta_file=${in_cat_dir}/EQT_20221220_20230103_mendo_stations.txt

min_lat=39.5
max_lat=41.5
min_lon=-125
max_lon=-123

reg=-R${min_lon}/${max_lon}/${min_lat}/${max_lat}
proj=-JM15
echo ${reg}
lat_lon_spacing=0.5
#lat_lon_spacing=0.1

region_inset=-R-125/-113/31/43
projection_inset=-JM1.5i

# Cross section parameters
strike1_angle=66
strike1_length=30
strike1_proj_width=8
strike1_center_lat=40.55
strike1_center_lon=-124.2
#strike1_bproj=-JX30/15
#strike1_brange=-R-30/30/-30/0
strike1_bproj=-JX30/20
strike1_brange=-R-30/30/-40/0

strike2_angle=122
strike2_length=30
strike2_proj_width=8
strike2_center_lat=40.52
strike2_center_lon=-124.2
#strike2_bproj=-JX30/15
#strike2_brange=-R-30/30/-30/0
strike2_bproj=-JX30/20
strike2_brange=-R-30/30/-40/0

strike3_angle=122
strike3_length=30
strike3_proj_width=8
strike3_center_lat=40.6
strike3_center_lon=-124.1
#strike3_bproj=-JX30/15
#strike3_brange=-R-30/30/-30/0
strike3_bproj=-JX30/20
strike3_brange=-R-30/30/-40/0

strike4_angle=122
strike4_length=30
strike4_proj_width=8
strike4_center_lat=40.44
strike4_center_lon=-124.3
#strike4_bproj=-JX30/15
#strike4_brange=-R-30/30/-30/0
strike4_bproj=-JX30/20
strike4_brange=-R-30/30/-40/0


# Time slice ranges
FIRST_DAY=0 # 2022-12-20
#LAST_DAY=3 # 2022-12-23
###LAST_DAY=7 # 2022-12-27
LAST_DAY=14 # 2023-01-03


##### COLOR BY DEPTH #####
#gmt grdcut @earth_relief_03s -R-125/-123/39.5/41.5 -Gmendo_topo.nc -V

#gmt makecpt -Cinferno -I -T0/30/0.01 > ${out_color_file}
gmt makecpt -Cinferno -I -T15/30/0.01 > ${out_color_file}

#gmt begin ${out_map_eqt_hinv_depth_file}
gmt begin ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}f0.1g -BneWS #-U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G210 -Slightskyblue -Lg-124.0/39.7+c-123.9/39.7+w50+f+lkm
#   gmt grdimage ${proj} ${reg} mendo_topo.nc -Cmap_gray.cpt
   gmt grdimage ${proj} ${reg} mendo_topo.nc -CGMT_globe_mod.cpt
   gmt coast ${proj} ${reg} -Na -Lg-124.0/39.7+c-123.9/39.7+w50+f+lkm

   gmt plot ${proj} ${reg} ${in_fault_file} -Sf1/0.01+l -W0.01c,black -G0
   gmt plot ${proj} ${reg} ${in_qfault_file} -Sf1/0.01+l -W0.01c,black -G0
   gmt plot ${proj} ${reg} ${in_offshore_fault_file} -Sf1/0.01+l -W0.01c,black -G0

   # Mainshock M6.4 hypocenter from ComCat
   gmt plot ${proj} ${reg} -Sa0.9 -W0.01c -Gyellow -BneWS -: << EOF
40.525 -124.423 17.9 6.4
EOF

   awk '{print $2 " " $3 " " $4 " " $1}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.25 -W0.05c,black -Gwhite -:
   awk '{print $2 " " $3 " " $1}' ${in_sta_file} | gmt text ${proj} ${reg} -D0/0.25 -F+f8p,Helvetica -:

   awk '{print $2 " " $3 " " $1}' ${in_city_file} | gmt plot ${proj} ${reg} -Ss0.3 -W0.05c,black -Gblack -:
   awk '{print $2 " " $3 " " $1}' ${in_city_file} | gmt text ${proj} ${reg} -D-0.5/0.3 -F+f10p,Helvetica -:

   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $5*0.2*0.2}' | gmt plot ${proj} ${reg} -Sc -W0.8+cl -BneWS -: -C${out_color_file}
#   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $4, $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Sc -W0.8+cl -BneWS -: -C${out_color_file}

   # Moment tensor - selected
   sort -nk1,1 ${in_mt_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($2/86400.) >= FD && ($2/86400.) <= LD) print $3, $4, $5, $6*0.2*0.2}' | gmt plot ${proj} ${reg} -Sc -W0.8+cl -BneWS -: -C${out_color_file}
   sort -nk1,1 ${in_mt_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $20 >= MT) print $3, $4, sqrt($5*$5), $9, $10, $11, $12, $13, $14, $15,     $17, $18, $19}' | gmt meca ${proj} ${reg} -Sm0.6c+f10 -A -: -C${out_color_file}

   gmt colorbar -C${out_color_file} -Dx16.0c/6c+w8c/0.5c+jMR -Bxaf+l"Depth (km)"

    # Project seismicity along one direction
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print -124.423, 40.525, 17.9, 200010, 6.4, 38068.66}' ${in_cat_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_plot_dir}/mainshock_seis_proj${strike1_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print -124.423, 40.525, 17.9, 200010, 6.4, 38068.66}' ${in_cat_file} | gmt project -Q -C${strike4_center_lon}/${strike4_center_lat} -A${strike4_angle} -W-${strike4_proj_width}/${strike4_proj_width} -L-${strike4_length}/${strike4_length} > ${out_plot_dir}/mainshock_seis_proj${strike4_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print -124.188, 40.523, 26.6, 200014, 4.9, 38342.36}' ${in_cat_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_plot_dir}/nc73821046_seis_proj${strike1_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print -124.188, 40.523, 26.6, 200014, 4.9, 38342.36}' ${in_cat_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_plot_dir}/nc73821046_seis_proj${strike2_angle}.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print -123.971, 40.409, 30.6, 204246, 5.4, 1103704.51}' ${in_cat_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_plot_dir}/nc73827571_seis_proj${strike2_angle}.txt # plot all events

## Default projection
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1, $7}' ${in_cat_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike1_angle}_A.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1, $7}' ${in_cat_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike2_angle}_C.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1, $7}' ${in_cat_file} | gmt project -Q -C${strike3_center_lon}/${strike3_center_lat} -A${strike3_angle} -W-${strike3_proj_width}/${strike3_proj_width} -L-${strike3_length}/${strike3_length} > ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike3_angle}_B.txt # plot all events
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1, $7}' ${in_cat_file} | gmt project -Q -C${strike4_center_lon}/${strike4_center_lat} -A${strike4_angle} -W-${strike4_proj_width}/${strike4_proj_width} -L-${strike4_length}/${strike4_length} > ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike4_angle}_D.txt # plot all events

### HYPOSVI projection
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1, $10}' ${in_cat_file} | gmt project -Q -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -W-${strike1_proj_width}/${strike1_proj_width} -L-${strike1_length}/${strike1_length} > ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike1_angle}_A.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1, $10}' ${in_cat_file} | gmt project -Q -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -W-${strike2_proj_width}/${strike2_proj_width} -L-${strike2_length}/${strike2_length} > ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike2_angle}_C.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1, $10}' ${in_cat_file} | gmt project -Q -C${strike3_center_lon}/${strike3_center_lat} -A${strike3_angle} -W-${strike3_proj_width}/${strike3_proj_width} -L-${strike3_length}/${strike3_length} > ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike3_angle}_B.txt # plot all events
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $3, $2, $4, $6, $5, $1, $10}' ${in_cat_file} | gmt project -Q -C${strike4_center_lon}/${strike4_center_lat} -A${strike4_angle} -W-${strike4_proj_width}/${strike4_proj_width} -L-${strike4_length}/${strike4_length} > ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike4_angle}_D.txt # plot all events

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W0.8,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f16 << EOF
-123.8 40.67 A'
-124.6 40.4 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W0.8,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f16 << EOF
-124.55 40.68 C
-123.85 40.35 C'
EOF
   gmt project -Q -G1 -C${strike3_center_lon}/${strike3_center_lat} -A${strike3_angle} -L-${strike3_length}/${strike3_length} | gmt plot ${proj} ${reg} -W0.8,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f16 << EOF
-124.45 40.75 B
-123.75 40.45 B'
EOF
   gmt project -Q -G1 -C${strike4_center_lon}/${strike4_center_lat} -A${strike4_angle} -L-${strike4_length}/${strike4_length} | gmt plot ${proj} ${reg} -W0.8,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f16 << EOF
-124.65 40.58 D
-124.0 40.25 D'
EOF

# Plot event magnitude legend
# https://docs.generic-mapping-tools.org/6.1/gallery/ex22.html
gmt gmtset FONT_ANNOT 12p
cat > neis.legend <<- END
V 0 1p
#N 7
N 1
V 0 1p
S 0.5c c 0.04c - 0.5p 1.0c M 1
S 0.5c c 0.08c - 0.5p 1.0c M 2
S 0.5c c 0.12c - 0.5p 1.0c M 3
S 0.5c c 0.16c - 0.5p 1.0c M 4
S 0.5c c 0.20c - 0.5p 1.0c M 5
S 0.5c c 0.24c - 0.5p 1.0c M 6
S 0.5c i 0.3c white 1.1p 1.0c station
S 0.5c s 0.3c black 0.8p 1.0c city
S 0.5c a 0.4c yellow 0.8p 1.0c mainshock
V 0 1p
N 1
END
gmt legend -DJBC+o9.4c/-18c+w3.4c/4.8c -F+p+gwhite neis.legend
gmt gmtset FONT_ANNOT 20p

#S 0.75c c 0.16c - 0.5p 1.5c M 1
#S 0.75c c 0.32c - 0.5p 1.5c M 2
#S 0.75c c 0.48c - 0.5p 1.5c M 3
#S 0.75c c 0.64c - 0.5p 1.5c M 4
#S 0.75c c 0.80c - 0.5p 1.5c M 5
#S 0.75c c 0.96c - 0.5p 1.5c M 6

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show


###### COLOR BY TIME #####
#
gmt makecpt -Cviridis -I -T${FIRST_DAY}/${LAST_DAY}/0.01 > ${out_color_time_file}
#gmt makecpt -Cviridis -Icz -T${FIRST_DAY}/${LAST_DAY}/0.01 > ${out_color_time_file}

gmt begin ${out_map_eqt_hinv_time_file}_${FIRST_DAY}_${LAST_DAY}
   gmt basemap ${proj} ${reg} -Ba${lat_lon_spacing}f0.1g -BneWS #-U
   gmt coast ${proj} ${reg} -W0.25p,black -Na -G210 -Slightskyblue -Lg-124.0/39.7+c-123.9/39.7+w50+f+lkm
   gmt grdimage ${proj} ${reg} mendo_topo.nc -CGMT_globe_mod.cpt
   gmt coast ${proj} ${reg} -Na -Lg-124.0/39.7+c-123.9/39.7+w50+f+lkm

   gmt plot ${proj} ${reg} ${in_fault_file} -Sf1/0.01+l -W0.01c,black -G0
   gmt plot ${proj} ${reg} ${in_qfault_file} -Sf1/0.01+l -W0.01c,black -G0
   gmt plot ${proj} ${reg} ${in_offshore_fault_file} -Sf1/0.01+l -W0.01c,black -G0

   # Mainshock M6.4 hypocenter from ComCat
   gmt plot ${proj} ${reg} -Sa0.9 -W0.01c -Gyellow -BneWS -: << EOF
40.525 -124.423 17.9 6.4
EOF

   awk '{print $2 " " $3 " " $4}' ${in_sta_file} | gmt plot ${proj} ${reg} -Si0.25 -W0.05c,black -Gwhite -:
   awk '{print $2 " " $3 " " $1}' ${in_sta_file} | gmt text ${proj} ${reg} -D0/0.25 -F+f8p,Helvetica -:
#   gmt colorbar -C${out_color_time_file} -Dx6.4i/2.4i/2.5i/0.15i -Bxaf+l"Days since "${start_date}

   awk '{print $2 " " $3 " " $1}' ${in_city_file} | gmt plot ${proj} ${reg} -Ss0.3 -W0.05c,black -Gblack -:
   awk '{print $2 " " $3 " " $1}' ${in_city_file} | gmt text ${proj} ${reg} -D-0.5/0.3 -F+f10p,Helvetica -:

   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $5*0.2*0.2}' | gmt plot ${proj} ${reg} -Sc -W0.8+cl -BneWS -: -C${out_color_time_file}
#   sort -nk1,1 ${in_cat_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($1/86400.) >= FD && ($1/86400.) <= LD) print $2, $3, $1/86400., $7/6.26172 + 0.03}' | gmt plot ${proj} ${reg} -Sc -W0.8+cl -BneWS -: -C${out_color_time_file}

   # Moment tensor - selected
   sort -nk1,1 ${in_mt_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($2/86400.) >= FD && ($2/86400.) <= LD) print $3, $4, $2/86400., $6*0.2*0.2}' | gmt plot ${proj} ${reg} -Sc -W0.8+cl -BneWS -: -C${out_color_time_file}
   sort -nk1,1 ${in_mt_file} | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} -v MT=${mag_thresh} '{if (($2/86400.) >= FD && ($2/86400.) <= LD && $20 >= MT) print $3, $4, $2/86400., $9, $10, $11, $12, $13, $14, $15,     $17, $18, $19}' | gmt meca ${proj} ${reg} -Sm0.6c+f10 -W0.01c -A -: -C${out_color_time_file}

   gmt colorbar -C${out_color_time_file} -Dx16.0c/6c+w8c/0.5c+jMR -Bxaf+l"Days since 2022-12-20"

   # Projection lines
   gmt project -Q -G1 -C${strike1_center_lon}/${strike1_center_lat} -A${strike1_angle} -L-${strike1_length}/${strike1_length} | gmt plot ${proj} ${reg} -W0.8,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f16 << EOF
-123.8 40.67 A'
-124.6 40.4 A
EOF
   gmt project -Q -G1 -C${strike2_center_lon}/${strike2_center_lat} -A${strike2_angle} -L-${strike2_length}/${strike2_length} | gmt plot ${proj} ${reg} -W0.8,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f16 << EOF
-124.55 40.68 C
-123.85 40.35 C'
EOF
   gmt project -Q -G1 -C${strike3_center_lon}/${strike3_center_lat} -A${strike3_angle} -L-${strike3_length}/${strike3_length} | gmt plot ${proj} ${reg} -W0.8,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f16 << EOF
-124.45 40.75 B
-123.75 40.45 B'
EOF
   gmt project -Q -G1 -C${strike4_center_lon}/${strike4_center_lat} -A${strike4_angle} -L-${strike4_length}/${strike4_length} | gmt plot ${proj} ${reg} -W0.8,0/0/0,-. -Gblack
   gmt text ${proj} ${reg} -F+f16 << EOF
-124.65 40.58 D
-124.0 40.25 D'
EOF

# Plot event magnitude legend
# https://docs.generic-mapping-tools.org/6.1/gallery/ex22.html
gmt gmtset FONT_ANNOT 12p
cat > neis.legend <<- END
V 0 1p
#N 7
N 1
V 0 1p
S 0.5c c 0.04c - 0.5p 1.0c M 1
S 0.5c c 0.08c - 0.5p 1.0c M 2
S 0.5c c 0.12c - 0.5p 1.0c M 3
S 0.5c c 0.16c - 0.5p 1.0c M 4
S 0.5c c 0.20c - 0.5p 1.0c M 5
S 0.5c c 0.24c - 0.5p 1.0c M 6
S 0.5c i 0.3c white 1.1p 1.0c station
S 0.5c s 0.3c black 0.8p 1.0c city
S 0.5c a 0.4c yellow 0.8p 1.0c mainshock
V 0 1p
N 1
END
gmt legend -DJBC+o9.4c/-18c+w3.4c/4.8c -F+p+gwhite neis.legend
gmt gmtset FONT_ANNOT 20p

   # Add inset
   gmt basemap ${region_inset} ${projection_inset} -Bnews -X0.05i -Y0.05i
   gmt coast ${region_inset} ${projection_inset} -Dl -G255 -Na -W1
   gmt plot ${region_inset} ${projection_inset} -W2,red << EOF
${min_lon} ${min_lat}
${min_lon} ${max_lat}
${max_lon} ${max_lat}
${max_lon} ${min_lat}
${min_lon} ${min_lat}
EOF
gmt end show

gmt gmtset LABEL_FONT_SIZE 32p
gmt gmtset FONT_ANNOT 32p
#gmt gmtset LABEL_FONT_SIZE 56p #poster
#gmt gmtset FONT_ANNOT 56p #poster

#gmt begin ${out_map_eqt_hinv_time_file}_seis_proj${strike1_angle}
gmt begin ${out_map_eqt_hinv_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike1_angle}_A
   gmt basemap ${strike1_bproj} ${strike1_brange} -Ba10f5 -BneWS+g170 #-U

   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $6/86400., $5*0.6*0.6}' ${out_plot_dir}/mainshock_seis_proj${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sa1.5 -W0.01c -Gyellow
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $6/86400., $5*0.6*0.6}' ${out_plot_dir}/nc73821046_seis_proj${strike1_angle}.txt | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.5+cl -C${out_color_time_file}
   sort -nk6,6 ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike1_angle}_A.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike1_bproj} ${strike1_brange} -Bxa10+l"Length along cross-section A-A' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.5+cl -C${out_color_time_file}
   
   gmt colorbar -C${out_color_time_file} -Dx32.0c/6c+w8c/0.5c+jMR -Bxaf+l"Days since 2022-12-20"
gmt end show

#gmt begin ${out_map_eqt_hinv_time_file}_seis_proj${strike2_angle}
gmt begin ${out_map_eqt_hinv_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike2_angle}_C
   gmt basemap ${strike2_bproj} ${strike2_brange} -Ba10f5 -BneWS+g170 #-U

#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $6/86400., $5*0.6*0.6}' ${out_plot_dir}/nc73821046_seis_proj${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa10+l"Length along cross-section C-C' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.5+cl -C${out_color_time_file}
#   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $6/86400., $5*0.6*0.6}' ${out_plot_dir}/nc73827571_seis_proj${strike2_angle}.txt | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa10+l"Length along cross-section C-C' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.5+cl -C${out_color_time_file}
   sort -nk6,6 ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike2_angle}_C.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike2_bproj} ${strike2_brange} -Bxa10+l"Length along cross-section C-C' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.5+cl -C${out_color_time_file}

   gmt colorbar -C${out_color_time_file} -Dx32.0c/6c+w8c/0.5c+jMR -Bxaf+l"Days since 2022-12-20"
gmt end show

gmt begin ${out_map_eqt_hinv_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike3_angle}_B
   gmt basemap ${strike3_bproj} ${strike3_brange} -Ba10f5 -BneWS+g170 #-U
   sort -nk6,6 ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike3_angle}_B.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike3_bproj} ${strike3_brange} -Bxa10+l"Length along cross-section B-B' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.5+cl -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx32.0c/6c+w8c/0.5c+jMR -Bxaf+l"Days since 2022-12-20"
gmt end show

gmt begin ${out_map_eqt_hinv_time_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike4_angle}_D
   gmt basemap ${strike4_bproj} ${strike4_brange} -Ba10f5 -BneWS+g170 #-U
   awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $7, $3*(-1.0), $6/86400., $5*0.6*0.6}' ${out_plot_dir}/mainshock_seis_proj${strike4_angle}.txt | gmt plot ${strike4_bproj} ${strike4_brange} -Bxa10+l"Length along cross-section D-D' (km)" -Bya10+l"Depth (km)" -BneWS -Sa1.5 -W0.01c -Gyellow
   sort -nk6,6 ${out_map_eqt_hinv_depth_file}_${FIRST_DAY}_${LAST_DAY}_seis_proj${strike4_angle}_D.txt | awk -v FD=${FIRST_DAY} -v LD=${LAST_DAY} '{if (($6/86400.) >= FD && ($6/86400.) <= LD) print $8, $3*(-1.0), $6/86400., $7}' | gmt plot ${strike4_bproj} ${strike4_brange} -Bxa10+l"Length along cross-section D-D' (km)" -Bya10+l"Depth (km)" -BneWS -Sc -W1.5+cl -C${out_color_time_file}
   gmt colorbar -C${out_color_time_file} -Dx32.0c/6c+w8c/0.5c+jMR -Bxaf+l"Days since 2022-12-20"
gmt end show

