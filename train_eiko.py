#dv='cuda:0'
dv='cuda'

# EikoNet
from EikoNet import model    as md
from EikoNet import database as db
from EikoNet import plot     as pt 

## HypoSVI
#from HypoSVI import location as lc
#
## Downloading files from Google Drive
#from google_drive_downloader import GoogleDriveDownloader as gdd
#
#
## Additional Pacakges for projections etc
#from IPython.display import Image
#from IPython.display import display
#from pyproj import Proj
#import pandas as pd
#import numpy as np
from glob import glob

import math
import numpy as np
import pandas as pd
import torch
import torch.autograd as autograd
import torch.optim as optim
import matplotlib.pylab as plt
import seaborn as sns


#PATH = '/media/yoon/INT01/Ferndale2022/EQT_20221220_20230103/Eiko_out'
##
##xmin               = [-124.7,40.2,-2] #Lat,Long,Depth
##xmax               = [-123.7,40.8,41] #Lat,Long,Depth
##projection         = "+proj=utm +zone=10, +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"
#
#xmin               = [-125.0,39.5,-2] #Lat,Long,Depth
#xmax               = [-123.0,41.5,41] #Lat,Long,Depth
#projection         = "+proj=utm +zone=10 +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"

#----------ENTIRE MTJ REGION---------------
PATH = '/media/yoon/INT01/Ferndale2022/EQT_20211201_20230601/Eiko_out_MTJ'
#xmin               = [-128.0,39.0,-2] #Lat,Long,Depth
#xmax               = [-122.0,44.0,41] #Lat,Long,Depth
xmin               = [-126.0,39.5,-2] #Lat,Long,Depth
xmax               = [-123.0,41.5,41] #Lat,Long,Depth
projection         = "+proj=utm +zone=10 +north +ellps=WGS84 +datum=WGS84 +units=km +no_defs"

vp_dir = 'VP_MENDO'
vp_file = vp_dir+'/VP_MENDO.csv'
vs_dir = 'VS_MENDO'
vs_file = vs_dir+'/VS_MENDO.csv'

# ------------- VP Velocity Model ----------
# Loading the CSV for viewing
###VP = pd.read_csv('{}/VP/VP.csv'.format(PATH),names=['Depth(km)','Velocity(km/s)']) # Don't need this line just for looking
#VP = pd.read_csv('{}/'+vp_file.format(PATH),names=['Depth(km)','Velocity(km/s)']) # Don't need this line just for looking

# Defining the Velocity Model region of interest
###vm_vp = db.Graded1DVelocity('{}/VP/VP.csv'.format(PATH),xmin=xmin,xmax=xmax,projection=projection)
vm_vp = db.Graded1DVelocity(('{}/'+vp_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)

# Plotting the interpolated velocity model
###vm_vp.plot('{}/VP/VP.png'.format(PATH))
vm_vp.plot(('{}/'+vp_dir+'/VP.png').format(PATH))

###model_VP  = md.Model('{}/VP'.format(PATH),vm_vp,device=dv)
model_VP  = md.Model(('{}/'+vp_dir).format(PATH),vm_vp,device=dv)

model_VP.Params['Training']['Number of Epochs']   = 16 # For the 1D velocity 10 epochs could be overkill
model_VP.Params['Training']['Save Every * Epoch'] = 1 # For the 1D velocity 10 epochs could be overkill
model_VP.train()

###vm_vp.plot_TestPoints(model_VP,'{}/VP/VP_TestPoints.png'.format(PATH))
###MEMORY###vm_vp.plot_TestPoints(model_VP,('{}/'+vp_dir+'/VP_TestPoints.png').format(PATH))

# VS EikoNet Training

# Constructing a Velocity model function 
###vm_vs = db.Graded1DVelocity('{}/VS/VS.csv'.format(PATH),xmin=xmin,xmax=xmax,projection=projection)
vm_vs = db.Graded1DVelocity(('{}/'+vs_file).format(PATH),xmin=xmin,xmax=xmax,projection=projection)

# Plotting the interpolated velocity model
###vm_vs.plot('{}/VS/VS.png'.format(PATH))
vm_vs.plot(('{}/'+vs_dir+'/VS.png').format(PATH))

###model_VS  = md.Model('{}/VS'.format(PATH),vm_vs,device=dv)
model_VS  = md.Model(('{}/'+vs_dir).format(PATH),vm_vs,device=dv)

model_VS.Params['Training']['Number of Epochs']   = 16 # For the 1D velocity 10 epochs could be overkill
model_VS.Params['Training']['Save Every * Epoch'] = 1 # For the 1D velocity 10 epochs could be overkill
model_VS.train()

###vm_vs.plot_TestPoints(model_VS,'{}/VS/VS_TestPoints.png'.format(PATH))
###MEMORY###vm_vs.plot_TestPoints(model_VS,('{}/'+vs_dir+'/VS_TestPoints.png').format(PATH))

